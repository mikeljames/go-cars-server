const expect = require('chai').expect;
const crypto = require('crypto');
const shasum = crypto.createHash('sha1');
shasum.update('a65dc6e99d4e212e28b4de65da106cdb');
expect(shasum.digest('hex')).to.equal('528709875eaec145a15041c994b2e7b02986896b')