const DBNAME = 'go-cars';
const cradle = require('cradle');
const connection = new(cradle.Connection)();
const db = connection.database(DBNAME);

db.destroy(() => {
  console.log('cleared db', DBNAME);
  const newDb = connection.database(DBNAME);
  newDb.create(() => {
    console.log('created db', DBNAME);
  });
});


const redis = require('redis');
const client = redis.createClient();
client.flushall(() => {
  console.log('cleared redis');
  client.quit();
});
