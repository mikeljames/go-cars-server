###Testing modes:
 - ```npm run start:test``` for integration testing of the server in isolation:
  this runs the server in a test mode with TEST_MODE env variables flag.
  spins up tdispatch stub api for development on port 3002.
  server runs on 3001.

  then once output in console says ```server Listening on PORT 3001``
  run: ```npm test:integration``` in another shell

- ```npm run seed:voice``` run this to seed a database on couch for integration testing on the voice server whilst in another shell have running server in test mode ```start:test```. 
