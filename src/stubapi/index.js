const Hapi = require('hapi');
const Joi = require('joi');
const randomString = require('random-string');
const Boom = require('boom');

const server = new Hapi.Server();
const port = 3002;
server.connection({
  port: port
});

const pks = {};
const keys = {};
const phones = {};

const wrapInBullShit = (key, payload) => ({[key]: payload, status: 'OK', status_code: 200});

server.route({
  method: 'POST',
  path: '/passenger/v1/bookings',
  handler: (request, reply) => {
    const key = randomString({length: 6});
    const pk = randomString({length: 24});
    const status = 'on_way_to_job';
    const payload = {
      'accessibilities': {},
      'account': {
        'name': 'GoCarsLimited gocars-kiosk',
        'pk': '4fd84da32b4e88ce3eb2659a'
      },
      'allocatedHours': null,
      'alternativeRoute': null,
      'autoDispatch': false,
      'avoid': {
        'highways': false,
        'tolls': false
      },
      'bookingKey': key,
      'cardToken': null,
      'cost': {
        'currency': 'GBP',
        'value': 3.8
      },
      'currentStatus': status,
      'distance': {
        'km': 0,
        'miles': 0
      },
      'driverPk': null,
      'dropoffLocation': null,
      'duration': {
        'minutes': 0,
        'seconds': 0
      },
      'extraInstructions': '',
      'extraPassengers': [],
      'extras': [],
      'flightNumber': '',
      'isPaid': false,
      'key': `${key}`,
      'luggage': 0,
      'masterKey': `GCB635-${key}`,
      'office': {
        'name': 'gocabs',
        'shortcode': 'GCB635',
        'slug': 'GCB635'
      },
      'passengerEmail': '',
      'passengerName': request.payload.passenger.name,
      'passengerPhone': request.payload.passenger.phone,
      'passengers': request.payload.passengers,
      'paymentMethod': 'cash',
      'paymentRef': null,
      'paymentStatus': null,
      'pickupAsap': true,
      'pickupLocation': {
        'address': 'Royal Parade 2, BS23 1SE Weston-super-Mare',
        'country': '',
        'doorNumber': '2',
        'location': {
          'lat': 51.3477064,
          'lng': -2.983022
        },
        'name': 'Mitey Bite Chip Shop, 9-11 Regent Street',
        'postcode': 'BS23 1SE',
        'street': 'Royal Parade',
        'town': 'Weston-super-Mare'
      },
      'pickupTime': request.payload.pickup_time,
      pk,
      'priceCorrection': null,
      'receiptUrl': 'bookings/Ns392S/receipt',
      status,
      'totalCost': {
        'currency': 'GBP',
        'value': 3.8
      },
      'vehicleType': null,
      'voucher': null,
      'wayPoints': []
    };

    pks[pk] = payload;
    keys[pk] = payload;
    phones[request.payload.passenger.phone] = payload;

    reply(wrapInBullShit('booking', payload));
  },
  config: {
    validate: {
      params: {
        access_token: Joi.string()
      }
    }
  }
});

server.route({
  method: 'POST',
  path: '/passenger/v1/bookings/{pk}/cancel',
  handler: (request, reply) => {

    const booking = pks[request.params.pk];
    if (!booking) {
      return reply(Boom.notFound());
    }

    reply({'status': 'OK', 'status_code': 200});
  },
  config: {
    validate: {
      query: {
        access_token: Joi.string()
      },
      params: {
        pk: Joi.string().min(24)
      },
      payload: {
        description: Joi.string().allow('').optional()
      }
    }
  }
});

server.route({
  method: 'GET',
  path: '/passenger/v1/bookings/{pk}',
  handler: (request, reply) => {
    const booking = pks[request.params.pk];
    if (!booking) {
      return reply(Boom.notFound());
    }
    reply(wrapInBullShit('booking', booking));
  },
  config: {
    validate: {
      query: {
        access_token: Joi.string()
      },
      params: {
        pk: Joi.string().min(24)
      }
    }
  }
});

server.route({
  method: 'POST',
  path: '/passenger/v1/accounts',
  config: {
    validate: {
      query: {
        key: Joi.string(),
        client_id: Joi.string().email()
      },
      payload: {
        first_name: Joi.string(),
        last_name: Joi.string(),
        email: Joi.string().email(),
        phone: Joi.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"'),
        password: Joi.string(),
        client_id: Joi.string().email()
      }
    }
  },
  handler: (request, reply) => {
    reply({
      passenger: {
        access_token: 'jB0g8u7o6CBT0NMEUonxh7BI',
        refresh_token: 'hVYRfQeceHIQi3rHZOOtEfPVX9m4xSXW'
      }
    });
  }
});

// auth code
server.route({
  method: 'POST',
  path: '/passenger/oauth2/auth',
  config: {
    validate: {
      query: {
        response_type: Joi.string().regex(/code/),
        client_id: Joi.string().email(),
        key: Joi.string(),
        response_format: Joi.string().regex(/json/),
        scope: Joi.string().allow('').optional()
      },
      payload: {
        username: Joi.string().email(),
        password: Joi.string()
      }
    },
    handler: (request, reply) => {
      reply({
        auth_code: 'Fm2JJmaNZYDEHNyvM4qTT2Ir',
        status: 'OK',
        status_code: 200
      });
    }
  }
});

server.route({
  method: 'POST',
  path: '/passenger/oauth2/token',
  config: {
    validate: {
      payload: Joi.object().keys({
        code: Joi.string().optional(),
        redirect_url: Joi.string().valid('').optional(), // optional wasn't working
        refresh_token: Joi.string().optional(),
        grant_type: Joi.string().regex(/authorization_code|refresh_token/).required(),
        client_secret: Joi.string().required(),
        client_id: Joi.string().email().required()
      })
    }
  },
  handler: (request, reply) => {
    if (request.payload.grant_type === 'refresh_token') {
      return reply({
        'access_token': 'jB0g8u7o6CBT0NMEUonxh7BI',
        'expires_in': 2591419,
        'status': 'OK',
        'status_code': 200,
        'token_type': 'Bearer'
      });
    }

    reply({
      'access_token': 'jB0g8u7o6CBT0NMEUonxh7BI',
      'expires_in': 2591850,
      'refresh_token': 'hVYRfQeceHIQi3rHZOOtEfPVX9m4xSXW',
      'status': 'OK',
      'status_code': 200,
      'token_type': 'Bearer'
    });
  }
});

server.start((err) => {
  if (err) {
    console.log('Error starting server on port', port, 'err: ', err);
    process.exit(1);
  }

  console.log('started stub api on ', port);
});

const exit = () => {
  console.log('shutting down stubapi');
  process.exit(0);
};

process.on('SIGINT', exit);
process.on('SIGTERM', exit);
