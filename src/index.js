import 'newrelic';
import Hapi from 'hapi';

import {PORT, JWT_SECRET_KEY, NODE_ENV, AUTH_COOKIE_NAME, AUTH_COOKIE_TTL} from './config';
import routes from './routes';
import {validateToken} from './validateToken';
import {validateBasic} from './validateBasic';
const isTestMode = process.env.TEST_MODE === 'true';
console.log('isTestMode', isTestMode);

const server = new Hapi.Server();

server.connection({
  port: PORT,
  routes: {cors: {credentials: true}}
});

server.state(AUTH_COOKIE_NAME, {
  ttl: AUTH_COOKIE_TTL,
  isSecure: NODE_ENV === 'production',
  isHttpOnly: true,
  clearInvalid: false, // remove invalid cookies
  strictHeader: true, // don't allow violations of RFC 6265
  path: '/'
});

server.register([require('bell'), require('hapi-auth-jwt2'), require('hapi-auth-basic')], (err) => {

  if (err) {
    console.log(err);
    return process.exit(1);
  }

  // Declare an authentication strategy using the bell scheme
  // with the name of the provider, cookie encryption password,
  // and the OAuth client credentials.
  // server.auth.strategy('facebook', 'bell', {
  //   provider: 'facebook',
  //   password: 'fa23sadHKUCOPVdSDhdsJdkS!23',
  //   clientId: '1674324019478742',
  //   clientSecret: 'f0bb35b70f1b15be90265798fabf94c6',
  //   isSecure: NODE_ENV === 'production'
  // });
  server.auth.strategy('simple', 'basic', { validateFunc: validateBasic });
  server.auth.strategy('jwt', 'jwt',
  { key: JWT_SECRET_KEY,          // Never Share your secret key
    validateFunc: validateToken,
    cookieKey: 'jwt',           // validate function defined above
    verifyOptions: { algorithms: [ 'HS256' ] } // pick a strong algorithm
  });

  server.auth.default('jwt');

  server.route(routes);

  server.start((e) => {
    if (e) {
      console.log(e);
      return process.exit(1);
    }

    console.log('Server Listening on PORT', PORT);
  });
});

const exit = () => {
  process.exit(0);
};

process.on('SIGINT', exit);
process.on('SIGTERM', exit);
