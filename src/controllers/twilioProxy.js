import { VOICE_API } from '../config';
import * as Fleet from '../models/fleet';
import * as Location from '../models/location';
import * as User from '../models/user';
import Boom from 'boom';
import * as twilio from '../services/twilio';

export const searchNumbers = (request, reply) => {
  const {number, isoCode} = request.query;

  const {credentials: {accountId}} = request.auth;

  User.get(accountId)
  .then((user) => Fleet.get(user.fleetId))
  .then(fleet => {
    if (!fleet.twilio.accountSid) {
      return reply(Boom.badRequest('ACCOUNT_SID_NOT_SET'));
    }

    return twilio.searchNumber(
      fleet.twilio.accountSid,
      isoCode,
      number
    );
  })
  .then(reply)
  .catch(e => {
    reply(Boom.badImplementation());
    console.log(e.stack);
  });
};

export const purchaseNumber = (request, reply) => {
  const {credentials: {accountId}} = request.auth;

  User.get(accountId)
  .then((user) => Fleet.get(user.fleetId))
  .then(fleet => {
    if (!fleet.twilio.accountSid) {
      return reply(Boom.badRequest('ACCOUNT_SID_NOT_SET'));
    }

    const friendlyName = `${fleet.fleetName} ${request.payload.phoneNumber}`;

    return twilio.purchaseNumber({
      accountSid: fleet.twilio.accountSid,
      friendlyName,
      voiceUrl: process.env.NODE_ENV === 'development' ? 'https://voice.gocaller.co.uk/call' : `${VOICE_API}/tdispatch/call`,
      phoneNumber: request.payload.phoneNumber
    })
    .then(twilioNumber => {
      let promise = Promise.resolve();

      if (request.payload.locationId) {
        promise = Location.addDirectNumber(
          request.payload.locationId,
          twilioNumber
        );
      }

      return promise.then(() =>
        Fleet.addNewNumber(fleet._id, {
          ...twilioNumber,
          locationId: request.payload.locationId
        }))
        .then(() => twilioNumber);
    })
    .then((twilioNumber) => {
      if (!fleet.twilio.number) {
        return Fleet.update(fleet._id, {
          twilio: {
            ...fleet.twilio,
            number: twilioNumber.phoneNumber
          }
        });
      }
    });
  })
  .then(() => reply())
  .catch(e => {
    console.log(e);
    reply(Boom.badImplementation());
    console.log(e.stack);
  });
};

export const releaseNumber = (request, reply) => {
  const {credentials: {accountId}} = request.auth;

  User.get(accountId)
  .then((user) => Fleet.get(user.fleetId))
  .then(fleet => {
    if (!fleet.twilio.accountSid) {
      return reply(Boom.badRequest('ACCOUNT_SID_NOT_SET'));
    }

    return twilio.releaseNumber({
      accountSid: fleet.twilio.accountSid,
      numberSid: request.payload.sid
    })
    .then(() => Fleet.removeNumber(fleet._id, request.payload.sid));
  })
  .then(() => reply())
  .catch(e => {
    reply(Boom.badImplementation());
    console.log(e.stack);
  });
};

export const summary = (request, reply) => {
  const {credentials: {accountId}} = request.auth;

  User.get(accountId)
  .then((user) => Fleet.get(user.fleetId))
  .then(fleet => {
    if (!fleet.twilio.accountSid) {
      return reply(Boom.badRequest('ACCOUNT_SID_NOT_SET'));
    }

    return twilio.account(fleet.twilio.accountSid);
  })
  .then(reply)
  .catch(e => {
    reply(Boom.badImplementation());
    console.log(e.stack);
  });
};

export const getNumbers = (request, reply) => {
  const {credentials: {accountId}} = request.auth;

  User.get(accountId)
  .then((user) => Fleet.get(user.fleetId))
  .then(fleet => {
    if (!fleet.twilio.accountSid) {
      return reply(Boom.badRequest('ACCOUNT_SID_NOT_SET'));
    }
    return twilio.numbers(fleet.twilio.accountSid);
  })
  // return just the array and not all the other crap
  .then(result => result.incomingPhoneNumbers)
  .then(reply)
  .catch(e => {
    reply(Boom.badImplementation());
    console.log(e.stack);
  });
};
