import * as User from '../models/user';
import Boom from 'boom';
import {AUTH_COOKIE_NAME, JWT_SECRET_KEY, JWT_TOKEN_EXPIRY} from '../config';
import jwt from 'jsonwebtoken';
import session from '../services/session';
import Uuid from 'uuid';
export const create = (request, reply) => {
  const {fleetName, email} = request.payload;

  User.create(request.payload)
  .then(() => {
    reply();
  })
  .catch(e => {
    if (e.message === 'EMAIL_EXISTS') {
      return reply(Boom.conflict('Email Exists', email));
    }

    if (e.message === 'FLEET_NAME_EXISTS') {
      return reply(Boom.conflict('Fleet Exists', fleetName));
    }
    console.log(e);
    reply(Boom.badImplementation('Error creating new user', e));
  });
};

export const login = (request, reply) => {
  const {username, password} = request.payload;

  User.login(username, password)
  .then(user => {
    if (!user) {
      return reply(Boom.unauthorized('Please check your username or password'));
    }
    delete user.hash;
    const sessionId = Uuid.v4();
    const token = jwt.sign({accountId: user._id, user, sessionId}, JWT_SECRET_KEY, {expiresIn: JWT_TOKEN_EXPIRY});

    session.set(sessionId, user)
    .then(() => {
      reply(user).state(AUTH_COOKIE_NAME, token);
    })
    .catch(e => {
      console.log(e);
      reply(Boom.badImplementation());
    });
  })
  .catch(e => {
    console.log(e);
    reply(Boom.badImplementation());
  });

};

export const password = (request, reply) => {
  const {code} = request.params;
  console.log('code', code);
  session.get(code)
  .then((user) => {
    console.log('changing password', user);
    if (!user) {
      return reply(Boom.badRequest('email timed out'));
    }
    User.updatePassword(user._id, request.payload.password)
    .then(() => session.del(code))
    .then(() => reply());
  })
  .catch(e => {
    console.log('Error getting ${code} for password reset', e);
    reply(Boom.badImplementation());
  });
};

export const forgot = (request, reply) => {
  User.findByEmail(request.payload.username)
  .then((res) => {
    reply();
    if (res) {
      User.sendForgotEmail(res)
      .catch(e => console.log('sendForgotEmail ERR', e));
    }
  })
  .catch(e => {
    console.log('findByEmail ERR', e);
    reply(Boom.badImplementation());
  });

};

export const get = (request, reply) => {
  const {credentials: {user: {_id}}} = request.auth;

  User.getAccount(_id)
  .then(reply)
  .catch(e => {
    console.log('User.getAccount ERR', e);
    reply(Boom.badImplementation());
  });
};

export const logout = (request, reply) => {
  const cookie = request.state[AUTH_COOKIE_NAME];
  const {credentials: {sessionId}} = request.auth;
  session.del(sessionId);
  if (!cookie) {return reply();}

  reply({message: 'You\'ve been successfully logged out'}).state(AUTH_COOKIE_NAME, '', {
    ttl: -1,
    isHttpOnly: true
  });
};

export const activeUsers = (request, reply) => {
  User.activeUsers()
  .then(reply)
  .catch(err => {
    console.log(err);
    reply(Boom.badImplementation(err));
  });
};
