import * as Fleet from '../models/fleet';
import {userHasActivatedFleet} from '../models/user';
import Boom from 'boom';

export const post = (request, reply) => {
  const {payload} = request;
  const {accountId} = request.auth.credentials;

  return Fleet.save(payload)
  .then((fleetSaved) => {
    return userHasActivatedFleet(accountId, fleetSaved)
    .then(() => ({id: fleetSaved.id}));
  })
  .then(result => {
    delete result.secret;
    reply(result);
  })
  .catch((err) => {
    console.log(err);
    reply(Boom.badImplementation('Failed to save new fleet'));
  });
};

export const getFleetForPublicConsumption = (request, reply) => {
  const { params: { id } } = request;

  Fleet.get(id)
  .then((fleet) => {
    if (!fleet) {
      return reply(Boom.notFound());
    }

    const omitted = {
      fleetName: fleet.fleetName,
      number: fleet.twilio ? fleet.twilio.number : null,
      language: fleet.language,
      voice: fleet.twilio.voice
    };

    reply(omitted);
  })
  .catch((err) => {
    if (err.error === 'not_found') {
      return reply(Boom.notFound('Fleet not found'));
    }
    console.log('Error getting fleet for public consumption', err.stack);
    reply(Boom.badImplementation());
  });
};

export const getById = (request, reply) => {
  Fleet.get(request.params.id)
  .then(fleet => {
    reply(fleet);
  })
  .catch(e => {
    console.log(e.stack);
    reply(Boom.badImplementation());
  });
};

export const put = (request, reply) => {
  const {id} = request.params;
  Fleet.get(id)
  .then((fleet) =>
    Fleet.update(id, {
      ...request.payload,
      // ensure twilio data is not lost as its a nested structure :(
      twilio: {
        ...fleet.twilio,
        ...request.payload.twilio
      }
    })
  )
  .then((fleet) => reply(fleet))
  .catch(e => {
    if (e.error === 'not_found') {
      return reply(Boom.badRequest('Fleet Id Not Found'));
    }
    console.log(e);
    reply(Boom.badImplementation(e));
  });
};

export const getAll = (request, reply) => {
  Fleet.getAll()
  .then(reply)
  .catch(e => reply(Boom.badImplementation(e)));
};

export const getByCalled = (request, reply) => {
  const {called} = request.params;
  Fleet.getByCalled(called)
  .then(reply)
  .catch(e => {
    if (e.error === 'not_found') {
      return reply();
    }

    console.log('Fleet.getByCalled ERR', e);
    reply(Boom.badImplementation());
  });
};
