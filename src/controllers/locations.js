import * as Location from '../models/location';
import * as Fleet from '../models/fleet';
import Boom from 'boom';
import twoFactorCall from '../services/twoFactorCall';
import Session from '../services/session';
import * as User from '../models/user';
import _ from 'lodash';
export const post = (request, reply) => {
  const location = request.payload;
  const fleetId = request.params.fleetId;

  Fleet.get(fleetId)
  .then((fleet) => {
    if (!fleet) {
      return reply(Boom.badRequest('fleet not found'));
    }

    location.fleetId = fleetId;

    Location.save(location)
    .then((locationSaved) => {
      twoFactorCall(fleet, locationSaved)
      .then(() => console.log(`calling ${location.telephoneNumber}`))
      .catch((e) => console.log(`error calling ${location.telephoneNumber}`, e));

      reply(locationSaved);
    })
    .catch((err) => {
      console.log('Location save ERR', err);
      reply(Boom.badImplementation());
    });

  })
  .catch((err) => {
    if (err.notFound) {
      return reply(Boom.notFound(`Fleet not found id:${fleetId}`));
    }

    console.log('fetching fleet ERR', err);
    reply(Boom.badImplementation());
  });
};

export const assignNumber = (request, reply) => {
  Location.addDirectNumber(request.params.id, request.payload.directNumber)
  .then(() => reply())
  .catch(e => {
    console.log(e.stack);
    reply(Boom.badImplementation());
  });
};

export const addNewLocationWithAuthentication = (request, reply) => {
  const location = request.payload;
  const fleetId = request.params.fleetId;

  Fleet.get(fleetId)
  .then((fleet) => {
    if (!fleet) {
      return reply(Boom.badRequest('fleet not found'));
    }

    location.fleetId = fleetId;

    Location.save({...location, isVerified: true})
    .then(reply)
    .catch((err) => {
      console.log('Location save ERR', err);
      reply(Boom.badImplementation());
    });

  })
  .catch((err) => {
    if (err.notFound) {
      return reply(Boom.notFound(`Fleet not found id:${fleetId}`));
    }

    console.log('fetching fleet ERR', err);
    reply(Boom.badImplementation());
  });
};

export const getAll = (request, reply) => {
  Location.all()
  .then(locations => {
    const locationsToShow = locations.map(l =>
      _.omit(l, ['fleetId', 'bookings', '_id'])
    );

    reply(locationsToShow);
  })
  .catch(err => {
    console.log('ERROR fetching all locations', err);
    reply(Boom.notFound()); // not found for obscurity.
  });
};

export const getByTelephoneNumber = (request, reply) => {
  Location.getByTelephoneNumber(request.params.number)
  .then(location => {
    if (location) {
      reply(location);
    } else {
      reply(Boom.notFound());
    }
  })
  .catch(err => {
    console.log(err);
    reply(Boom.badImplementation());
  });

};

export const verify = (request, reply) => {
  const {id} = request.params;
  const {oneTimeCode} = request.payload;

  Session.get(`${id}-${oneTimeCode}`)
  .then((result) => {
    if (!result) {
      return reply(Boom.badRequest('Invalid one time code'));
    }

    return Location.verify(id)
    .then(() => {
      reply();
    });
  })
  .catch(e => {
    console.log(e);
    reply(Boom.badImplementation(e));
  });
};

export const getAllForFleet = (request, reply) => {
  const {credentials: {user: {_id}}} = request.auth;
  // we get user first as they may not have the fleet id in their token as it could be their first login.
  User.get(_id)
  .then(({fleetId}) => {
    if (!fleetId) {
      return reply([]);
    }

    Location.getAllForFleet(fleetId)
    .then(reply);
  })
  .catch(e => {
    if (e.error === 'not_found') {
      return reply([]);
    }
    console.log('Location.getAllForFleet ERR', e);
    reply(Boom.badImplementation());
  });
};

export const update = (request, reply) => {
  const {params: {locationId}, payload, auth: {credentials: {user: {_id}}}} = request;
  Promise.all([
    User.get(_id),
    Location.get(locationId)])
  .then(([user, location]) => {
    if (user.fleetId !== location.fleetId) {
      return reply(Boom.badRequest('You do not have permission to change this location', locationId));
    }

    Location.update(locationId, payload)
    .then(() => reply())
    .catch(e => {
      console.log('Location.update ERR', e);
      reply(Boom.badImplementation());
    });

  })
  .catch(e => {
    console.log('Location.update, Promise.all, User.get, Location.get ERR', e);
    reply(Boom.badImplementation());
  });
};

export const del = (request, reply) => {
  const {params: {locationId}, auth: {credentials: {user: {_id}}}} = request;
  Promise.all([
    User.get(_id),
    Location.get(locationId)])
  .then(([user, location]) => {
    if (user.fleetId !== location.fleetId) {
      return reply(Boom.badRequest('You do not have permission to change this location', locationId));
    }
    Location.del(locationId)
    .then(() => reply())
    .catch(e => {
      console.log('Location.del ERR', e);
      reply(Boom.badImplementation());
    });
  })
  .catch(e => {
    console.log('Location.del, Promise.all, User.get, Location.get ERR', e);
    reply(Boom.badImplementation());
  });
};
