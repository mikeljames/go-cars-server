import { getByTelephoneNumber as getLocationAndBookings } from '../models/location';
import {
  getByCalled as fleetByTelephoneNumber,
  get as getFleet
} from '../models/fleet';
import Boom from 'boom';

export default function(request, reply) {
  const {caller, called} = request.payload;
  // called being a 121 number caller being the user making the call
  // use this for the users booking session data
  getLocationAndBookings(called, caller)
  .then(location => {
    if (!location.name) {
      // called is not a 121 number as it doesnt return a location
      // lookup by caller for location
      // and lookup fleet by called
      return Promise.all([
        getLocationAndBookings(caller),
        fleetByTelephoneNumber(called)
      ])
      .then(([loc, fleet]) => ({
        location: loc,
        fleet,
        isOneToOne: false
      }))
      .catch(() => {
        return {};
      });
    }
    // caller is making call at a known location {tel: called}
    return getFleet(location.fleetId)
    .then(fleet => ({
      location,
      fleet,
      isOneToOne: true
    }))
    .catch(() => {
      return {};
    });
  })
  .then(identifed => reply(identifed))
  .catch(e => {
    console.log(e.stack);
    reply(Boom.badImplementation());
  });
}
