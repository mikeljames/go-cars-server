/* eslint no-use-before-define:0 */
import {decrypt} from '../services/td-webhook';
import updateCaller from '../services/UpdateCaller';
import * as Fleet from '../models/fleet';
import {getBookingFromWebhookPayload} from '../models/booking';

export default (request, reply) => {
  const encryptedData = request.payload.payload;
  const decryptMode = request.query.decryptMode && process.env.TEST_MODE === 'true' ? false : true;

  const fleetHash = request.payload['fleet-hash'];

  console.log('payload', JSON.stringify(request.payload));

  reply(); // we don't care about response to tdispatch

  Fleet.getFleetByKeyHash(fleetHash)
  .then((fleet) => {
    let promise = null;

    if (decryptMode) promise = decrypt(encryptedData, fleet.webhookKey);
    else promise = Promise.resolve(encryptedData);

    return promise
    // we need to request t-dispatch per webhook request to get the full booking record
    // we store bookings based on telephoneNumber in redis
    .then(getBookingFromWebhookPayload(fleet))
    // pass booking from previous request to updateCaller
    .then((booking) => {
      if (booking) {
        return updateCaller(fleet, booking);
      }
    });
    // .catch(e => console.log(`Error on decrypt update caller fleet: ${fleet.id} Error:`, e));

  })
  .catch((e) => console.log(`Error getFleet`, e));
};
