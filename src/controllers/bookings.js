import * as Booking from '../models/booking';
import * as Location from '../models/location';
import * as Fleet from '../models/fleet';
import Boom from 'boom';

export const book = ({payload, params}, reply) => {
  // map timezone to fleet timezone
  // fleet timezone set by: moment.tz().names()
  const booking = payload;
  const {locationId} = params;
  Location.getByShortId(locationId)
  .then((location) => {
    if (!location) {
      return reply(Boom.notFound());
    }

    Fleet.get(location.fleetId)
    .then((fleet) => {
      booking.address = location.address;
      booking.postcode = location.postcode;
      booking.coords = location.coords;
      booking.extraInstructions = location.extraInstructions;
      booking.telephoneNumber = booking.telephoneNumber || location.telephoneNumber;

      Booking.book({booking, fleet})
      .then((bookingReceipt) => {
        reply(bookingReceipt);
      })
      .catch((err) => {
        console.log('BOOKING ERR', err);
        reply(Boom.badImplementation());
      });
    })
    .catch((err) => {
      console.log('FLEET FETCH ERR', err);
      reply(Boom.badImplementation());
    });

  })
  .catch((err) => {
    if (err.notFound) {
      return reply(Boom.notFound());
    }
    console.log('LOCATION getByShortId ERR', err);
    reply(Boom.badImplementation());
  });
};

export const cancel = (request, reply) => {
  const {params: {pk}, payload: {telephoneNumber, description}} = request;
  console.log(request.payload);
  Booking.cancel({pk, telephoneNumber, description})
  .then(reply)
  .catch(e => {
    console.log('Booking.cancel ERR', e, 'pk', pk);
    reply(Booking.badImplementation());
  });
};
