import { VOICE_PASSWORD} from './config';
import bcrypt from 'bcrypt';
const users = {
  voice: {
    name: 'voice_user',
    username: 'voice',
    password: VOICE_PASSWORD
  }
};

export const validateBasic = (request, username, password, callback) => {

  const user = users[username];
  if (!user) {
    return callback(null, false);
  }

  bcrypt.compare(password, user.password, (err, isValid) => {
    if (err) {
      console.log('login error', username, password, user.password);
    }

    callback(err, isValid, {
      name: user.name
    });
  });
};
