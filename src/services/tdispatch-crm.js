import request from 'request-promise';
import camelCaseKeys from 'camelcase-keys-recursive';
import { Tdispatch } from '../config';
import Url from 'url';

export const create = ({
  key, clientId
}, customer) => {

  const url = Url.resolve(Tdispatch.url, `/passenger/v1/accounts?key=${key}&client_id=${clientId}`);

  return request.post({
    url: url,
    body: {
      first_name: customer.firstName,
      last_name: customer.lastName,
      email: customer.email,
      phone: customer.phone,
      password: customer.password,
      client_id: clientId
    },
    json: true
  })
  .then(camelCaseKeys)
  .catch(e => {
    console.log(e.message);
    throw e;
  });
};
