import twilio from 'twilio';
import { VOICE_API, TWILIO_AUTH_KEY } from '../config';
import session from './session';
import randomString from 'random-string';

const isTestMode = process.env.TEST_MODE === 'true';

export default (fleet, location) => {
  const oneTimeCode = isTestMode ? '1234' : randomString({
    length: 4,
    numeric: true,
    letters: false,
    special: false
  });

  const {
    accountSid, authToken, number
  } = fleet.twilio;

  // console.log('setting oneTimeCode', `${oneTimeCode}`);
  return session.set(`${location.id}-${oneTimeCode}`, location.id, 600)
    .then(() => {
      // ensure we can still make calls for non twilio connect users
      return twilio(accountSid, fleet.twilioConnect ? TWILIO_AUTH_KEY : authToken).calls.create({
        url: `${VOICE_API}/location/verify?oneTimeCode=${oneTimeCode}`,
        to: location.telephoneNumber,
        from: number,
        method: 'POST'
      });
    });
};
