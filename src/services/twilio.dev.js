import { TWILIO_AUTH_KEY } from '../config';
import twilio from 'twilio';
console.log('TWILIO DEV MODE IS ENABLED');
export const searchNumber = (accountSid, isoCode, number) =>
  twilio(accountSid, TWILIO_AUTH_KEY)
  .availablePhoneNumbers(isoCode)
  .local
  .list({contains: number})
  .then((data) => data.availablePhoneNumbers)
  .catch(() => []);

const random = () => Math.floor(Math.random() * 10);
const purchased = [
  {
    phoneNumber: '+528153511646',
    sid: '1231231243hj1h23jh1231231'
  }
  // {
  //   phoneNumber: '+4419344154232',
  //   sid: '1231231243hj1h23jh4531231'
  // },
  // {
  //   phoneNumber: '+4419344154872',
  //   sid: '1231231243hj1h23jh4538731'
  // }
];

export const purchaseNumber = ({phoneNumber}) =>
  Promise.resolve({
    sid: '1231231243hj1h23jh123' + random(),
    phoneNumber: phoneNumber
  })
  .then((n) => {
    purchased.push(n);
    return n;
  });

export const releaseNumber = () =>
  Promise.resolve();

export const account = accountSid =>
  twilio(accountSid, TWILIO_AUTH_KEY)
  .accounts(accountSid)
  .get();

export const numbers = () =>
  Promise.resolve({
    incomingPhoneNumbers: purchased
  });
