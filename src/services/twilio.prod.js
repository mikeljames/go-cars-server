import { TWILIO_AUTH_KEY } from '../config';
import twilio from 'twilio';

export const searchNumber = (accountSid, isoCode, number) =>
  twilio(accountSid, TWILIO_AUTH_KEY)
  .availablePhoneNumbers(isoCode)
  .local
  .list({contains: number})
  .then((data) => data.availablePhoneNumbers)
  .catch(() => []);

export const purchaseNumber = ({
  accountSid,
  friendlyName,
  voiceUrl,
  phoneNumber
}) =>
  twilio(accountSid, TWILIO_AUTH_KEY)
  .incomingPhoneNumbers
  .create({
    phoneNumber,
    friendlyName,
    voiceUrl
  });

export const releaseNumber = ({
  accountSid,
  numberSid
}) =>
  twilio(accountSid, TWILIO_AUTH_KEY)
  .incomingPhoneNumbers(numberSid)
  .delete();

export const account = accountSid =>
  twilio(accountSid, TWILIO_AUTH_KEY)
  .accounts(accountSid)
  .get();

export const numbers = accountSid =>
  twilio(accountSid, TWILIO_AUTH_KEY)
  .incomingPhoneNumbers
  .list();
