import twilio from 'twilio';
import session from './session';
import { VOICE_API, TWILIO_AUTH_KEY } from '../config';
import {types} from 'tdispatch-contrib-booking-types';
import moment from 'moment';

function bookingDriverExistsAndPreviousStatusNoDriver(booking, bookingStoredLocal) {
  return booking.driverPk && !bookingStoredLocal.driverPk;
}

function previousAndNewHaveDrivers(booking, bookingStoredLocal) {
  return booking.driverPk && bookingStoredLocal.driverPk;
}

function driversAreNotTheSame(booking, bookingStoredLocal) {
  return bookingStoredLocal.driverPk.id !== booking.driverPk.id;
}

function previousAndNextStatusAreDifferent(booking, bookingStoredLocal) {
  return bookingStoredLocal.status !== booking.status;
}

function isPhoneCallStatus(booking) {
  return booking.status === types.ARRIVED_WAITING || booking.status === types.ON_THE_WAY || booking.status === types.MISSED;
}

function updateStoredLocalStatus(bookingStoredLocal, booking, tel, bookings) {
  bookingStoredLocal.status = booking.status; // pass by reference yuk!
  return session.set(tel, bookings);
}

function removeEndOfSessionBookings(bookings) {
  return bookings.filter(b => b.status !== types.CANCELLED || b.status !== types.COMPLETED);
}

export default (fleet, {booking}) => {

  return new Promise((resolve, reject) => {
    const tel = booking.passengerPhone.replace('+', '');
    return session.get(tel)
      .then((bookings) => {

        if (!bookings) {
          resolve();
        }
        console.log('bookings.find', bookings);
        console.log('typeof bookings', bookings);

        const bookingStoredLocal = bookings.find(b => b.key === booking.key);

        if (!bookingStoredLocal) {
          return resolve();
        }
        console.log('passengerPhone', booking.passengerPhone, 'status', booking.status);
        console.log('bookingStoredLocal.status', bookingStoredLocal.status);
        console.log('booking.status', booking.status);

        // if bookingLocal.driver.
        if (isPhoneCallStatus(booking)) {
          console.log('is phone call status');

          if (bookingDriverExistsAndPreviousStatusNoDriver(booking, bookingStoredLocal) ||
                previousAndNewHaveDrivers(booking, bookingStoredLocal)
                && driversAreNotTheSame(booking, bookingStoredLocal)
              ) {
            console.log('bookings have different drivers');

            if (previousAndNextStatusAreDifferent(booking, bookingStoredLocal)) {
              console.log('making phone call');

              return makePhoneCall(fleet, booking)
              .then(() => updateStoredLocalStatus(bookingStoredLocal, booking, tel, bookings));
            }
          }
        }

        return updateStoredLocalStatus(bookingStoredLocal, booking, tel, removeEndOfSessionBookings(bookings));
      })
      .catch(e => {
        console.log(`Failed to fetch bookings for ${booking.passengerPhone}`);
        reject(e);
      });

  });

};

function makePhoneCall(fleet, booking) {
  const twilioCredentials = fleet.twilio;
  const passengers = booking.passengers;
  const status = booking.status;
  const pickupTime = moment.utc(booking.pickupTime).format();
  const pk = booking.pk;

  return new Promise((resolve, reject) => {
    console.log('twilioCredentials', twilioCredentials);
    const client = twilio(twilioCredentials.accountSid, fleet.twilioConnect ? TWILIO_AUTH_KEY : twilioCredentials.authToken);
    const callbackUrl = `${VOICE_API}/status/callback?status=${status}&passengers=${passengers}&pickupTime=${pickupTime}&pk=${pk}`;
    console.log('payload', callbackUrl, 'to ', booking.passengerPhone, 'from', twilioCredentials.number);
    client.calls.create({
      url: callbackUrl,
      to: booking.passengerPhone,
      from: twilioCredentials.number,
      method: 'POST'
    }, (err) => {

      if (err) {
        return reject(err);
      }

      resolve();

    });


  });
}
