import crypto from 'crypto';
import camelCaseKeys from 'camelcase-keys-recursive';

export const decrypt = (data, secret) => {
  return new Promise((resolve, reject) => {
    const payload = new Buffer(data, 'base64').toString('binary');
    const iv = payload.substr(0, 16);
    const encryptedData = payload.substr(16);

    try {
      const decipher = crypto.createDecipheriv('aes-128-cbc', secret, iv);

      decipher.setAutoPadding(false);

      let dec = decipher.update(encryptedData, 'binary', 'utf8');

      dec += decipher.final('utf8');
      console.log(dec);
      dec = JSON.parse(dec.toString().replace(/\*/g, ''));

      resolve(camelCaseKeys(dec));

    } catch (e) {
      reject(e);
    }
  });
};

export const encrypt = (data, secret) => {
  const clearEncoding = 'utf8';
  const cipherEncoding = 'hex';
  const cipher = crypto.createCipheriv('aes-128-cbc', secret, crypto.randomBytes(16));
  const cipherChunks = [];
  cipher.setAutoPadding(false);
  cipherChunks.push(cipher.update(data, clearEncoding, cipherEncoding));

  const byteLength = Buffer.byteLength(cipherChunks.join(''), 'hex');
  console.log('byteLength before padding', byteLength);
  // if (byteLength > 128) {
  //   if (byteLength > 128) {

  //     let padding = '';
  //     for (let i = 0; i < (byteLength - 128)/8; i++) {
  //       padding += '*';
  //     }

  //     console.log('padding length', Buffer.byteLength(padding, 'hex'));
  //     cipherChunks.push(padding, clearEncoding, cipherEncoding);

  //   }
  // }

  if (byteLength < 128) {

    let padding = '';

    for (let i = 0; i < 128 - byteLength; i++) {
      padding += '*';
    }
    console.log('padding length', Buffer.byteLength(padding, 'hex'));

    cipherChunks.push(cipher.update(padding, clearEncoding, cipherEncoding));
  }

  console.log('length after padding', Buffer.byteLength(cipherChunks.join(), 'hex'));
  const fin = cipher.final(cipherEncoding);

  console.log('fin length', Buffer.byteLength(fin, 'hex'));

  return fin;
};
