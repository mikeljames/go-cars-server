import mailer, {activateTemplate} from './Email';

mailer({
  to: 'm@mikejam.es',
  subject: '🚕  Welcome to gocaller.co.uk! Please confirm your email address',
  html: activateTemplate('https://www.gocaller.co.uk')
})
.then((res) => {
  console.log(res);
})
.catch(err => {
  console.log(err);
});
