import redis from 'redis';
import {CACHE_TTL} from '../config';

let client;

if (process.env.NODE_ENV === 'production') {
  client = redis.createClient(process.env.REDIS_URL);
} else {
  client = redis.createClient();
}

function set(key, data, ttl = CACHE_TTL) {
  return new Promise((resolve, reject) => {
    // console.log('set', key, data);
    client.set(key, JSON.stringify(data), (err) => {
      if (err) return reject(err);

      resolve(key);

      client.expire(key, ttl);
    });

  });
}


function get(key) {
  return new Promise((resolve, reject) => {
    client.get(key, (err, res) => {
      if (err) return reject(err);
      try {
        const parsed = JSON.parse(res);
        resolve(parsed);
      } catch (e) {
        console.log(e);
        resolve({});
      }
    });
  });
}

function del(key) {
  client.del(key);
  return Promise.resolve();
}

export default {
  set,
  get,
  del
};
