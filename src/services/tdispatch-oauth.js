import rp from 'request-promise';
import * as Config from '../config';
import Url from 'url';
import camelCaseKeys from 'camelcase-keys-recursive';

export default class TdispatchAuth {
  constructor(opts) {
    this.secret = opts.secret;
    this.key = opts.key;
    this.clientId = opts.clientId;
    this.username = opts.username;
    this.password = opts.password;
  }

  getAccessToken() {
    return this.getAuthCode()
    .then((code) => {
      return this.authenticate(code);
    });
  }

  getAuthCode() {
    const AccessCodeUrl = Url.resolve(Config.Tdispatch.url, 'passenger/oauth2/auth?response_type=code&client_id=' + this.clientId + '&key=' + this.key + '&response_format=json&scope=');

    return rp.post({
      uri: AccessCodeUrl,
      body: {
        username: this.username,
        password: this.password
      },
      json: true
    })
    .then(camelCaseKeys)
    .catch(e => {
      console.log('An error occured when gettingAuthCode', e.response);
      console.log('Response body: ', e.response.body);
    });
  }

  authenticate({authCode}) {
    return rp.post({
      uri: Url.resolve(Config.Tdispatch.url, '/passenger/oauth2/token'),
      body: {
        code: authCode,
        redirect_url: '',
        grant_type: 'authorization_code',
        client_secret: this.secret,
        client_id: this.clientId
      },
      json: true
    })
    .then(camelCaseKeys);

  }
}
