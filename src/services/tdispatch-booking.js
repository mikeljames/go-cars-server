'use strict';
const apiUrl = require('../config').Tdispatch.url;
const resolveUrl = require('url').resolve;
const bodyParser = require('./RequestBodyParser');
const curry = require('curry');
const getAccessToken = require('./Auth').getAccessToken;
const request = require('request');

const get = curry((bookingHookPayload, accessToken) => {

  const url = resolveUrl(apiUrl, `/passenger/v1/bookings/${bookingHookPayload.booking_pk}?access_token=${accessToken.access_token}`);

  return new Promise((resolve, reject) => {
    request.get(url, bodyParser(resolve, reject));
  });
});

const track = curry((bookingHookPayload, accessToken) => {
  const url = resolveUrl(apiUrl, `/passenger/v1/bookings/track?access_token=${accessToken.access_token}`);

  return new Promise((resolve, reject) => {
    request.post({
      url,
      headers: {
        'Content-Type': 'application/json'
      },
      timeout: 20000,
      body: JSON.stringify({
        booking_pks: [
          bookingHookPayload.booking_pk
        ]
      })
    }, bodyParser(resolve, reject));
  });
});

export const getBooking = curry((fleet, bookingHookPayload) => {
  const accessToken = getAccessToken({
    username: fleet.fleetCustomer.email,
    password: fleet.fleetCustomer.password,
    secret: fleet.secret,
    key: fleet.key,
    clientId: fleet.clientId
  });

  const getResult = accessToken.then(get(bookingHookPayload));

  const trackResult = accessToken.then(track(bookingHookPayload));

  return Promise.all([getResult, trackResult])
  .then(bookings => {
    return {
      booking: bookings[0],
      bookingStatus: bookings[1].bookings[0],
      fleet: fleet
    };
  });
});

