'use strict';
var Uuid = require('node-uuid');

var internals = {};
module.exports = internals.Guid = function(){
	
};

internals.Guid.generate = function(){
	return Uuid.v4();
};