import Boom from 'boom';

export default (roleExpected, handler) => (request, reply) => {
  const {user: {role}} = request.auth.credentials;

  if (role === roleExpected) {
    return handler(request, reply);
  }

  reply(Boom.notFound());
};
