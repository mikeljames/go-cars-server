import Cradle from 'cradle';
import * as Config from '../config';
import {promisify} from 'bluebird';
import Uuid from 'node-uuid';

let db;
if (process.env.NODE_ENV === 'development') {

  db = new(Cradle.Connection)();

} else {

  db = new(Cradle.Connection)(
    Config.couchDb.url,
    Config.couchDb.port, {
      auth: {
        username: Config.couchDb.username,
        password: Config.couchDb.password
      }
    });
}

db = db.database(Config.couchDb.bucket);

export default db;

export const couchSave = (id = Uuid.v4(), doc) => {
  return promisify(db.save.bind(db))(id, doc);
};

export const couchGet = promisify(db.get.bind(db));
export const couchTypeGet = (id, type) => couchGet(id).then(d => d.type === type ? d : null);
export const couchUpdate = promisify(db.merge.bind(db));
export const couchDelete = promisify(db.remove.bind(db));

export const couchView = (docId, queryOptions, optionalOpts = {value: false}) => {
  const promise = promisify(db.view.bind(db)).call(db, docId, queryOptions);
  if (optionalOpts.value) {
    return promise.then((res) => res.map(r => Object.assign({}, r, {id: r._id})));
  }

  return promise;
};
export const couchDesignDoc = (name, indexes) => { return couchSave(`_design/${name}`, indexes);};

// TODO couchDesignDoc abstraction returns an object containing the generated couchView methods,
// which can then be exposed in a module
