const crypto = require('crypto');

export default (k) => {
  const shasum = crypto.createHash('sha1');
  shasum.update(k);
  return shasum.digest('hex');
};
