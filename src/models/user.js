import { couchSave, couchTypeGet, couchView, couchDesignDoc, couchUpdate } from '../helpers/couch';
import bcrypt from 'bcrypt';
import { promisify } from 'bluebird';
import Session from '../services/session';
import Uuid from 'node-uuid';
import * as Fleet from './fleet';
import _ from 'lodash';
import sendEmail, { welcomeTemplate, forgotTemplate } from '../services/Email';

const isTestMode = process.env.TEST_MODE === 'true';

const comparePassword = promisify(bcrypt.compare);

const hashFn = promisify(bcrypt.hash);
const TYPE = 'User';

export const create = ({
  name, fleetName, email, password
}) => {

  return Promise.all([
    couchView(`${TYPE}/email`, {
      key: email
    }),
    couchView('Fleet/name', {
      key: fleetName
    })
  ])
  .then(([user, fleet]) => {

    if (user.length) {
      throw Error('EMAIL_EXISTS');
    }

    if (fleet.length) {
      throw Error('FLEET_NAME_EXISTS');
    }

    return hashFn(password, 10)
      .then((hash) => {
        const id = Uuid.v4();
        sendWelcomeEmail({name, fleetName, email});
        return couchSave(id, {
          name, fleetName, email, hash, role: 'fleet-admin', type: 'User'
        });
      });
  });

};

const sendWelcomeEmail = ({name, fleetName, email}) =>
  sendEmail({
    email,
    html: welcomeTemplate({
      name,
      fleetName,
      link: 'https://app.gocaller.co.uk'
    }),
    subject: '🚕  Welcome to Go Caller'
  });

export const sendForgotEmail = (user) => {
  const id = !isTestMode ? Uuid.v4() : '1a5ae654-6388-47f4-b7b4-641f01bfb248';
  console.log('ID for email', id);
  sendEmail({
    to: user.email,
    html: forgotTemplate({ link: `https://app.gocaller.co.uk/passwordreset/${id}` }),
    subject: '🚕  Forgot Password - Go Caller'
  });

  return Session.set(id, user);
  // .then(send email)
};

export const findByEmail = (key) => {
  return couchView(`${TYPE}/email`, {key}, {value: true})
  .then(([first]) => (first));
};

export const login = (username, password) => {
  return couchView(`${TYPE}/email`, {
    key: username
  })
  .then(([res]) => {
    if (!res) {
      return null;
    }

    const user = res.value;
    return comparePassword(password, user.hash)
      .then(comparison => {
        if (comparison) {
          return user;
        }
        return null;
      });
  });

};

export const userHasActivatedFleet = (userId, fleet) => {
  return couchUpdate(userId, {fleetId: fleet.id, fleetActivated: true});
};

export const update = (id, newProps) => {
  return couchUpdate(id, newProps);
};

export const updatePassword = (id, password) => {
  return hashFn(password, 10)
  .then((hash) => update(id, {hash}));
};

export const getAccount = (id) => {
  return get(id)
  .then(sanitizeUser)
  .then((user) => {
    if (!user.fleetId) {
      return Object.assign({}, user, {
        fleet: null,
        fleetId: null
      });
    }

    return Fleet.get(user.fleetId)
    .then(Fleet.sanitizeFleet)
    .then((fleet) => {
      return Object.assign({}, user, {
        fleet
      });
    });
  });
};

export const sanitizeUser = (user) => {
  return _.omit(user, ['hash']);
};

export const activeUsers = () => {
  return couchView(`${TYPE}/activeUsers`);
};

export const get = (id) => {
  return couchTypeGet(id, TYPE);
};

couchDesignDoc(TYPE, {
  email: {
    map: `function (doc) {
			if(doc.type === "${TYPE}") {
				emit(doc.email, doc);
			}
		}`
  },
  activeUsers: {
    map: `function (doc) {
      if(doc.type === "${TYPE}" && doc.fleetActivated) {
        emit(doc.id, doc);
      }
    }`
  }
});
