'use strict';
var Config = require('../config');
var Db = require('../helpers/db').createDbConnection().database(Config.couchDb.bucket);
var _ = require('lodash');
var Guid = require('../helpers/guid');
var internals = {};

module.exports = internals.Base = function(attr) {
	this.attr = attr;
	this.id = attr.id || attr._id || Guid.generate();
};

internals.Base.prototype.save = function() {
	var _this = this;

	var promise = new Promise(function(resolve, reject) {

		Db.save(_this.id, _this.toJSON(), function(err, res) {
			if (err) {
				return internals.errorHandler(reject,err);
			}

			resolve(_.extend(res, _this.toJSON()));
		});

	});

	return promise;

};

internals.Base.prototype.update = function(attr){
	var _this = this;

	var promise = new Promise(function(resolve, reject) {

		Db.merge(_this.id, attr, function(err, res) {
			if (err) {
				return internals.errorHandler(reject,err);
			}

			var updated = _.extend(_this.toJSON(), attr);

			resolve(_.extend(res, updated));
		});

	});

	return promise;
};

internals.Base.query = function(view, obj, Model){
	return new Promise(function(resolve, reject) {

		Db.view(Model.TYPE + '/'+view, obj, function(err, res){

			if(err){
				return reject(err);
			}
			var resCloned;

			if(res && _.isArray(res) && !res.length){
				reject({notFound:true});
			
			}else if(res && res.length){
			
				resCloned = _.clone(_.first(res).value);
				resCloned.id = res.id;
				resolve(new Model(resCloned));
			}else{
			
				reject('unrecognised response');
			}
		});

	});
};

internals.errorHandler = function(reject, err){
	if(err.error === 'not_found'){
		reject({notFound:true});
	}else{
		reject(err);
	}
};

internals.Base.get = function(id) {
	var Model = this;
	return new Promise(function(resolve, reject) {

		Db.get(id, {cacheEnabled:false}, function(err, res) {
			if(err) {
				return internals.errorHandler(reject,err);
			}

			resolve(new Model(res));
		});

	});

};