import randomString from 'random-string';
import Uuid from 'node-uuid';
import { getCurrentBookings } from './booking';
import {couchUpdate, couchSave, couchTypeGet, couchView, couchDesignDoc} from '../helpers/couch';
const TYPE = 'Location';

export const save = ({
  address,
  postcode,
  coords,
  extraInstructions,
  fleetId,
  bookings,
  shortId,
  isVerified,
  name,
  telephoneNumber
}) => {
  return couchSave(Uuid.v4(), {
    type: TYPE,
    address,
    postcode,
    coords,
    extraInstructions,
    fleetId,
    bookings: bookings || [],
    shortId: shortId || randomString({length: 6, numeric: true}),
    isVerified: isVerified || false,
    name,
    telephoneNumber,
    deleted: false
  })
  .then((result) => get(result.id))
  .then((location) => {
    return {
      ...location,
      id: location._id
    };
  });
};

export const getByShortId = (key) => {
  return couchView(`${TYPE}/getByShortId`, {key}, {value: true})
  .then(([first]) => (first));
};

export const getByTelephoneNumber = (locationNumber, oneToOneCallSessionNumber) => {
  // ensure it works for temporary users so they can hear a list of their own bookings
  if (oneToOneCallSessionNumber) {
    return Promise.all([
      couchView(`${TYPE}/getByTelephoneNumber`, {key: locationNumber}, {value: true}),
      getCurrentBookings(oneToOneCallSessionNumber)
    ])
    .then(([[location], bookings]) => {
      return {
        ...location,
        bookings
      };
    })
    .catch(() => {
      // return nothing as not found
      return {};
    });
  }
  return Promise.all([
    couchView(`${TYPE}/getByTelephoneNumber`, {key: locationNumber}, {value: true}),
    getCurrentBookings(locationNumber)
  ])
  .then(([[location], bookings]) => {
    return {
      ...location,
      bookings
    };
  })
  .catch(() => {
    return {};
  });
};

export const addDirectNumber = (id, number) =>
  couchUpdate(id, {
    directNumber: number.phoneNumber,
    sid: number.sid
  });

export const verify = (id) => {
  return couchUpdate(id, {isVerified: true});
};

// super-admin view
export const all = () => {
  return couchView(`${TYPE}/all`, null, {value: true});
};

export const getAllForFleet = (key) => {
  return couchView(`${TYPE}/getAllForFleet`, {key}, {value: true});
};

export const del = (id) => {
  return couchUpdate(id, {deleted: true});
};

export const get = key => couchTypeGet(key, TYPE);
export const update = couchUpdate;

couchDesignDoc(TYPE, {
  all: {
    map: `function(doc) {
      if (doc.type === "${TYPE}") {
        emit(doc.id, doc);
      }
    }`
  },
  getNonVerified: {
    map: `function(doc) {
      if (!doc.isVerified && doc.type === "${TYPE}") {
        emit(doc.id, doc);
      }
    }`
  },
  getByShortId: {
    map: `function(doc) {
      if (doc.type === "${TYPE}" && doc.isVerified && doc.deleted === false) {
        emit(doc.shortId, doc);
      }
    }`
  },
  getAllForFleet: {
    map: `function(doc) {
      if (doc.type === "${TYPE}" && doc.deleted === false && doc.isVerified) {
        emit(doc.fleetId, doc);
      }
    }`
  },
  getByTelephoneNumber: {
    map: `function(doc) {
      if (doc.type === "${TYPE}" && doc.isVerified && doc.deleted === false) {
        if (doc.directNumber) {
          emit(doc.directNumber, doc);
        }
        emit(doc.telephoneNumber, doc);
      }
    }`
  }
});
