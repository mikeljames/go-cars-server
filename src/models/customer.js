import {create} from '../services/tdispatch-crm';
import {couchSave, couchDesignDoc} from '../helpers/couch';
import Uuid from 'node-uuid';

const TYPE = 'Customer';

export const saveCustomer = (opts, customer) => {
  const {key, clientId} = opts;
  const {firstName, lastName, password, email} = customer;

  const payload = {
    email,
    firstName,
    lastName,
    password,
    key,
    clientId
  };

  const id = Uuid.v4();
  return create({key, clientId}, payload)
    .then(({passenger}) => {
      return couchSave(id, {
        email,
        firstName,
        lastName,
        password,
        key,
        clientId,
        type: TYPE
      })
      .then(() => passenger);
    });
};

couchDesignDoc(TYPE, {
  all: {
    map: `function(doc) {
      if (doc.type === "${TYPE}") {
        emit(doc.id, doc);
      }
    }`
  }
});
