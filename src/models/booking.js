import rp from 'request-promise';
import {couchDesignDoc} from '../helpers/couch';
import * as Config from '../config';
import Url from 'url';
import TdispatchAuth from '../services/tdispatch-oauth';
import camelCaseKeys from 'camelcase-keys-recursive';
import session from '../services/session';
import * as Fleet from './fleet';
import {CALL_CACHE_TTL} from '../config';

const TYPE = 'Booking';

// update to allow Oauth customers to book.
export const book = ({booking, fleet}) => {
  return auth(fleet)
  .then(_book(booking))
  .then((bookingResult) => {
    const {telephoneNumber} = booking;
    return storeToCache({
      telephoneNumber,
      booking: bookingResult,
      fleetId: fleet.id,
      locationId: booking.locationId
    })
    .then(() => bookingResult);
  });
};

const auth = (fleet) => {
  return new TdispatchAuth({
    username: fleet.defaultCustomer.email,
    password: fleet.defaultCustomer.password,
    secret: fleet.secret,
    key: fleet.key,
    clientId: fleet.clientId
  }).getAccessToken();
};

const _book = (booking) => ({accessToken}) => {

  const uri = Url.resolve(Config.Tdispatch.url, `/passenger/v1/bookings?access_token=${accessToken}`);
  return rp.post({
    uri,
    timeout: 20000, // Tdispatch can be slow.
    body: {
      passenger: {
        name: booking.passengerName,
        phone: booking.telephoneNumber
      },
      pickup_time: booking.pickUpTime,
      pickup_location: {
        postcode: booking.postcode,
        location: {
          lat: booking.coords.lat,
          lng: booking.coords.lng
        },
        address: booking.address
      },
      passengers: booking.passengerCount,
      extra_instructions: booking.extra_instructions,
      payment_method: 'cash',
      status: 'incoming',
      autoDispatch: true
    },
    json: true
  })
  .then(camelCaseKeys)
  .then((bookingResponse) => bookingResponse.booking); // tdispatch wraps its responses in 200 ok status codes etc. eurgh
};

const storeToCache = ({telephoneNumber, booking, fleetId}) => {
  const tel = telephoneNumber.replace('+', '');
  console.log('telephoneNumber', tel);
  return session.get(tel)
  .then((bookingsMade) => !Array.isArray(bookingsMade) ? [] : bookingsMade)
  .then((bookingsMade) => {
    console.log('booking.key', booking.key);
    console.log('bookingsMade', bookingsMade);
    // ensure booking doesn't exist already for any reason
    const existingBooking = bookingsMade.filter(b => b.key === booking.key);
    if (existingBooking.length) {
      console.log('booking exists in cache', booking.key);
      // booking exists in cache already so just resolve and forget
      return Promise.resolve();
    }
    const bookingsMadeUpdated = [...bookingsMade, Object.assign({}, booking, {
      fleetId
    })];

    return session.set(tel, bookingsMadeUpdated, CALL_CACHE_TTL);
  });
};

export const getCurrentBookings = (tel) => {
  return session.get(tel.replace('+', ''))
  .then(bookings => {
    if (!bookings) {
      return [];
    }
    return bookings;
  });
};

export const cancel = ({pk, telephoneNumber, description}) => {
  const tel = telephoneNumber.replace('+', '');
  return session.get(tel)
  .then((bookings) => {
    if (!bookings) throw new Error(`No Bookings for ${telephoneNumber}`);
    console.log('bookings', bookings);
    console.log('typeof bookings', typeof(bookings));
    const booking = bookings.find(b => b.pk === pk);
    if (!booking) throw new Error(`No Bookings for ${pk}`);

    return Fleet.get(booking.fleetId)
    .then(auth)
    .then(camelCaseKeys)
    .then(_cancel(pk, description))
    .then(() => {
      if (bookings.length === 1) {
        return session.del(tel);
      }
      const bookingsRecord = bookings.find(b => b.pk !== pk);
      return session.set(tel, bookingsRecord);
    });
  });
};

const _cancel = (pk, description) => ({accessToken}) => {
  const url = Url.resolve(Config.Tdispatch.url, `/passenger/v1/bookings/${pk}/cancel?access_token=${accessToken}`);
  return rp.post({
    url,
    headers: {
      'Content-Type': 'application/json'
    },
    timeout: 20000, // Tdispatch can be slow.
    body: {
      description
    },
    json: true
  })
  .then(camelCaseKeys);
};

export const getBookingFromWebhookPayload = fleet => webhookPayload => {
  return auth(fleet)
  .then(({accessToken}) => {
    return _getBooking(webhookPayload.bookingPk, accessToken)
    .then(camelCaseKeys);
  });
};

const _getBooking = (pk, accessToken) => {
  const uri = Url.resolve(Config.Tdispatch.url, `/passenger/v1/bookings/${pk}?access_token=${accessToken}`);
  return rp.get({
    uri,
    json: true
  });
};

// resolveUrl(apiUrl, `/passenger/v1/bookings/${bookingHookPayload.booking_pk}?access_token=${accessToken.access_token}`
// const get = (pk)

couchDesignDoc(TYPE, {
  all: {
    map: `function(doc) {
      if (doc.type === "${TYPE}") {
        emit(doc.id, doc);
      }
    }`
  }
});
