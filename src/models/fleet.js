import {couchSave, couchView, couchDesignDoc, couchDelete, couchUpdate, couchTypeGet} from '../helpers/couch';
import {saveCustomer} from './customer';
import {defaultCustomer} from '../config';
import sha1 from '../helpers/sha1';
import randomString from 'random-string';
import Uuid from 'node-uuid';
import _ from 'lodash';
const TYPE = 'Fleet';

export const findByName = (key) => {
  return couchView(`${TYPE}/name`, {key});
};

export const save = (fleet) => {
  return saveFleetCustomer(fleet)
  .then(saveFleet(fleet));
};

// const auth = (fleet) => {
//   return new TdispatchAuth({
//     username: fleet.defaultCustomer.email,
//     password: fleet.defaultCustomer.password,
//     secret: fleet.secret,
//     key: fleet.key,
//     clientId: fleet.clientId
//   }).getAccessToken();
// };https://github.com/TDispatch/Passenger-API/wiki/v1-ref-Office-Time

const random = () => Math.floor(Math.random() * 100) + Math.floor(Math.random() * 100);
const saveFleetCustomer = ({fleetName, key, clientId}) => {
  const customerPayload = {
    email: defaultCustomer.email.replace('{fleetName}', `${fleetName.replace(/\s/g, '')}-${random()}-${random()}`),
    firstName: fleetName,
    lastName: defaultCustomer.lastName,
    password: Uuid.v4()
  };

  return saveCustomer({
    key,
    clientId
  }, customerPayload)
  .then((doc) => ({...doc, ...customerPayload}));
};

const saveFleet = (fleet) => (customer) => {
  const keyHash = sha1(fleet.key);
  const payload = {
    ...fleet,
    twilio: {
      accountSid: fleet.twilio.accountSid,
      voice: 'alice'
    },
    refreshToken: customer.refreshToken,
    accessToken: customer.accessToken,
    keyHash,
    defaultCustomer: customer,
    twilioConnect: !!fleet.twilio.accountSid,
    webhookKey: randomString({length: 16}),
    type: TYPE
  };

  return couchSave(Uuid.v4(), payload);
};

export const getFleetByKeyHash = (key) => {
  return couchView(`${TYPE}/getByKeyHash`, {key}, {value: true})
  .then(([fleet]) => fleet);
};

export const getAll = () => {
  return couchView(`${TYPE}/all`);
};

export const update = (id, payload) => {
  if (payload.key) {
    payload.keyHash = sha1(payload.key);
  }

  console.log('update payload', payload);
  return couchUpdate(id, payload);
};

export const get = (key) => {
  return couchTypeGet(key, TYPE);
};

export const getByCalled = (key) => {
  return couchView(`${TYPE}/getByCalled`, {key}, {value: true})
  .then(([fleet]) => fleet);
};

export const sanitizeFleet = (fleet) => {
  return Object.assign({}, fleet, {
    defaultCustomer: _.omit(fleet.defaultCustomer, ['password'])
  });
};


export const addNewNumber = (fleetId, number) =>
  couchSave(number.phoneNumber, {
    ...number,
    fleetId,
    type: 'FLEET_NUMBER'
  });

export const getFleetNumbers = fleetId =>
  couchView(`FLEET_NUMBER/numbersForFleet`, {key: fleetId}, {value: true});

export const removeNumber = (fleetId, numberSid) =>
  couchView('FLEET_NUMBER/numberBySid', { key: numberSid }, { value: true })
  .then(numbers => {
    /* eslint-disable consistent-return */
    if (!numbers || !numbers.length) {
      return;
    }
    const number = numbers[0];
    const del = couchDelete(number.phoneNumber);

    if (number.locationId) {
      del.then(() => couchUpdate(number.locationId, {directNumber: null}));
    }

    return del;
  });

couchDesignDoc(TYPE, {
  all: {
    map: `function(doc) {
      if (doc.type === "${TYPE}") {
        emit(doc.id, doc);
      }
    }`
  },
  getByKeyHash: {
    map: `function(doc) {
      if (doc.type === "${TYPE}") {
        emit(doc.keyHash, doc);
      }
    }`
  },
  getByCalled: {
    map: `function(doc) {
      if (doc.type === "${TYPE}") {
        if(doc.twilio && doc.twilio.number) {
          emit(doc.twilio.number, doc);
        }
      }
    }`
  },
  name: {
    map: `function(doc) {
      if (doc.type === "${TYPE}") {
        emit(doc.fleetName, doc);
      }
    }`
  }
});

couchDesignDoc('FLEET_NUMBER', {
  numberBySid: {
    map: `function(doc) {
      if (doc.type === "FLEET_NUMBER") {
        emit(doc.sid, doc);
      }
    }`
  },
  numbersForFleet: {
    map: `function(doc) {
      if (doc.type === "FLEET_NUMBER") {
        emit(doc.fleetId, doc)
      }
    }`
  }
});
