'use strict';

import * as BookingsController from './controllers/bookings';
import * as FleetsController from './controllers/fleets';
import * as LocationsController from './controllers/locations';
import tdispatchWebhook from './controllers/tdispatch-webhook';
import * as Users from './controllers/users';
import Joi from 'joi';
import protect from './helpers/protect';
import { welcomeTemplate, forgotTemplate } from './services/Email';
import * as TwilioProxy from './controllers/twilioProxy';
import identifyCaller from './controllers/identifyCaller';

export default [
  {
    method: 'POST',
    path: '/fleet/user',
    handler: Users.create,
    config: {
      validate: {
        payload: {
          name: Joi.string().min(1).required(),
          fleetName: Joi.string().required(),
          email: Joi.string().email().required(),
          password: Joi.string().regex(/^[a-zA-Z0-9]{8,30}$/)
        }
      },
      auth: false
    }
  },

  {
    method: 'GET',
    path: '/fleet/user',
    handler: Users.get
  },

  {
    method: 'GET',
    path: '/fleet/{id}',
    handler: FleetsController.getById,
    config: {
      validate: {
        params: {
          id: Joi.string().guid()
        }
      },
      auth: 'simple'
    }
  },

  {
    method: 'GET',
    path: '/fleets/{id}',
    handler: FleetsController.getFleetForPublicConsumption,
    config: {
      validate: {
        params: {
          id: Joi.string().guid()
        }
      },
      auth: false
    }
  },

  {
    method: 'POST',
    path: '/fleet/user/login',
    handler: Users.login,
    config: {
      validate: {
        payload: {
          username: Joi.string().email(),
          password: Joi.string().regex(/^[a-zA-Z0-9]{8,30}$/)
        }
      },
      auth: false
    }
  },

  {
    method: 'POST',
    path: '/fleet/user/logout',
    handler: Users.logout,
    config: {
      auth: 'jwt'
    }
  },

  {
    method: 'PUT',
    path: '/fleet/user/password/{code}',
    handler: Users.password,
    config: {
      validate: {
        payload: {
          password: Joi.string().regex(/^[a-zA-Z0-9]{8,30}$/)
        },
        params: {
          code: Joi.string().guid()
        }
      },
      auth: false
    }
  },

  {
    method: 'POST',
    path: '/fleet/user/forgot',
    handler: Users.forgot,
    config: {
      validate: {
        payload: {
          username: Joi.string().email()
        }
      },
      auth: false
    }
  },
  // alternative we copy over the twilio proxy *
  // we preserve the current routes and structure *
  // by the user being shown twilio before anything else in app
  // we check for authToken in the model within the two factor calls *
  // client app shows twilio connect if new twilioConnect flag is falsy
  // client requests to post fleet setting twilio and t-dispatch setting twilioConnect flag to true *
  // if fleet.twilio.authToken exists we put to the api setting twilioConnect flag to true *
  // client continues normally
  // voice remains unchanged *
  // calls to twilio will need to check for *
  // if !fleet.twilio.twilioConnect then simply use old call method *
  // if fleet.twilio.twilioConnect then use new call method *
  // wait for users to pass by and update then remove the flag.
  // add unit tests to controllers
  {
    method: 'POST',
    path: '/fleet',
    handler: FleetsController.post,
    config: {
      validate: {
        payload: {
          key: Joi.string().min(32).max(32).required(),
          clientId: Joi.string().min(8).required(),
          secret: Joi.string().min(32).max(32).required(),
          fleetName: Joi.string().required(),
          twilio: Joi.object().keys({
            accountSid: Joi.string().required()
          }),
          appEnabled: Joi.bool().default(false),
          website: Joi.string().default(null),
          language: Joi.string()
            .regex(
              /en|da-DK|de-DE|en-AU|en-CA|en-GB|en-IN|en-US|ca-ES|es-ES|es-MX|fi-FI|fr-CA|fr-FR|it-IT|ja-JP|ko-KR|nb-NO|nl-NL|pl-PL|pt-BR|pt-PT|ru-RU|sv-SE|zh-CN|zh-HK|zh-TW/g,
              'valid language code eg. en-GB'
            )
            .default('en')
        }
      }
    }
  },

  {
    method: 'PUT',
    path: '/fleet/{id}',
    handler: FleetsController.put,
    config: {
      validate: {
        payload: {
          key: Joi.string().min(32).max(32),
          clientId: Joi.string().min(8),
          secret: Joi.string().min(32).max(32),
          twilio: Joi.object().keys({
            accountSid: Joi.string()
          }),
          voice: Joi.object().keys({
            greeting: Joi.string(),
            callback: Joi.string(),
            notRegistered: Joi.string()
          }),
          appEnabled: Joi.bool().default(false),
          website: Joi.string().default(null),
          language: Joi.string()
          .regex(/en|da-DK|de-DE|en-AU|en-CA|en-GB|en-IN|en-US|ca-ES|es-ES|es-MX|fi-FI|fr-CA|fr-FR|it-IT|ja-JP|ko-KR|nb-NO|nl-NL|pl-PL|pt-BR|pt-PT|ru-RU|sv-SE|zh-CN|zh-HK|zh-TW/g,
            'valid language code eg. en-GB'
          ).default('en')
        },
        params: {
          id: Joi.string().guid()
        }
      }
    }
  },

  {
    method: 'PATCH',
    path: '/fleet/{id}',
    handler: FleetsController.put,
    config: {
      validate: {
        params: {
          id: Joi.string().guid()
        }
      }
    }
  },

  {
    method: 'POST',
    path: '/fleets/{fleetId}/location',
    handler: LocationsController.post,
    config: {
      validate: {
        payload: {
          name: Joi.string(),
          address: Joi.string().min(8),
          postcode: Joi.string().min(5).max(7),
          coords: Joi.object().keys({
            lat: Joi.number(), // add range validation -90 - 90
            lng: Joi.number() // -180 - 180
          }),
          telephoneNumber: Joi.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"'),
          extraInfo: Joi.string().allow('').allow(null).default('')
        },
        params: {
          fleetId: Joi.string().guid()
        }
      },
      auth: false
    }
  },

  {
    method: 'POST',
    path: '/fleets/{fleetId}/locations/add',
    handler: LocationsController.addNewLocationWithAuthentication,
    config: {
      validate: {
        payload: {
          name: Joi.string(),
          address: Joi.string().min(8),
          postcode: Joi.string().min(5).max(7),
          coords: Joi.object().keys({
            lat: Joi.number(), // add range validation -90 - 90
            lng: Joi.number() // -180 - 180
          }),
          telephoneNumber: Joi.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"'),
          extraInfo: Joi.string().allow('').allow(null).default('')
        },
        params: {
          fleetId: Joi.string().guid()
        }
      }
    }
  },

  {
    method: 'PUT',
    path: '/fleet/locations/{locationId}',
    handler: LocationsController.update,
    config: {
      validate: {
        payload: {
          name: Joi.string(),
          address: Joi.string().min(8),
          postcode: Joi.string().min(5).max(7),
          coords: Joi.object().keys({
            lat: Joi.number(),
            lng: Joi.number()
          }),
          telephoneNumber: Joi.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"'),
          extraInfo: Joi.string().allow('').allow(null).default('')
        },
        params: {
          locationId: Joi.string().guid()
        }
      }
    }
  },

  {
    method: 'DELETE',
    path: '/fleet/locations/{locationId}',
    handler: LocationsController.del,
    config: {
      validate: {
        params: {
          locationId: Joi.string().guid()
        }
      }
    }
  },

  {
    method: 'GET',
    path: '/fleet/called/{called}',
    handler: FleetsController.getByCalled,
    config: {
      validate: {
        params: {
          called: Joi.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"')
        }
      },
      auth: 'simple'
    }
  },

  {
    method: 'GET',
    path: '/fleets',
    handler: protect('super-admin', FleetsController.getAll)
  },

  {
    method: 'GET',
    path: '/fleet/locations',
    handler: LocationsController.getAllForFleet
  },
  {
    method: 'GET',
    path: '/activeusers',
    handler: protect('super-admin', Users.activeUsers)
  },

  {
    method: 'GET',
    path: '/locations',
    handler: protect('super-admin', LocationsController.getAll)
  },

  {
    method: 'PATCH',
    path: '/location/{id}', // update to /location/verify/{id}
    handler: LocationsController.verify,
    config: {
      validate: {
        params: {
          id: Joi.string().guid()
        },
        payload: {
          oneTimeCode: Joi.string().min(4)
        }
      },
      auth: false
    }
  },

  {
    method: 'POST',
    path: '/book/{locationId}',
    handler: BookingsController.book,
    config: {
      validate: {
        payload: {
          passengerName: Joi.string().min(2).max(80),
          pickUpTime: Joi.date().iso(),
          passengerCount: Joi.number().integer().min(1).max(6),
          telephoneNumber: Joi.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"')
        },
        params: {
          locationId: Joi.string().min(6)
        }
      },
      auth: 'simple'
    }
  },

  {
    method: 'POST',
    path: '/booking/{pk}/cancel',
    handler: BookingsController.cancel,
    config: {
      validate: {
        payload: {
          telephoneNumber: Joi.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"'),
          description: Joi.string().allow('').default('Cancelled by GoCaller.co').optional()
        },
        params: {
          pk: Joi.string().min(24)
        }
      },
      auth: 'simple'
    }
  },

  {
    method: 'GET',
    path: '/location/telephone/{number}',
    handler: LocationsController.getByTelephoneNumber,
    config: {
      validate: {
        params: {
          number: Joi.string()
        }
      },
      auth: {
        strategies: ['simple']
      }
    }
  },

  {
    method: 'POST',
    path: '/identifycaller',
    handler: identifyCaller,
    config: {
      validate: {
        payload: {
          caller: Joi.string(),
          called: Joi.string()
        }
      },
      auth: {
        strategies: ['simple']
      }
    }
  },

  {
    method: 'POST',
    path: '/tdispatch/webhook',
    handler: tdispatchWebhook,
    config: {
      auth: false
    }
  },

  {
    method: 'GET',
    path: '/',
    handler: (request, reply) => {
      reply('OK');
    },
    config: {
      auth: false
    }
  },

  {
    method: 'GET',
    path: '/emails/welcome',
    handler: (request, reply) => {
      reply(welcomeTemplate({
        link: 'https://app.gocaller.co.uk',
        name: 'Mike James',
        fleetName: 'Mikes Cabs'
      }));
    },
    config: {
      auth: false
    }
  },

  {
    method: 'GET',
    path: '/emails/forgot',
    handler: (request, reply) => {
      reply(forgotTemplate({
        link: 'https://app.gocaller.co.uk/passwordreset/1a5ae654-6388-47f4-b7b4-641f01bfb248'
      }));
    },
    config: {
      auth: false
    }
  },

  {
    method: 'GET',
    path: '/fleet/twilio/numbers/search',
    config: {
      validate: {
        query: {
          isoCode: Joi.string().min(2).max(2),
          number: Joi.string()
        }
      }
    },
    handler: TwilioProxy.searchNumbers
  },

  {
    method: 'POST',
    path: '/fleet/twilio/numbers/purchase',
    config: {
      validate: {
        payload: {
          phoneNumber: Joi.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"'),
          locationId: Joi.string().guid().allow(null).default(null)
        }
      }
    },
    handler: TwilioProxy.purchaseNumber
  },

  {
    method: 'DELETE',
    path: '/fleet/twilio/numbers/release',
    config: {
      validate: {
        payload: {
          sid: Joi.string()
        }
      }
    },
    handler: TwilioProxy.releaseNumber
  },

  {
    method: 'GET',
    path: '/fleet/twilio/account',
    handler: TwilioProxy.summary
  },

  {
    method: 'GET',
    path: '/fleet/twilio/numbers',
    handler: TwilioProxy.getNumbers
  },

  {
    method: 'POST',
    path: '/location/{id}/number/assign',
    handler: LocationsController.assignNumber,
    config: {
      validate: {
        payload: {
          number: Joi.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"')
        }
      }
    }
  }
];
