import getEnv from 'getenv';

export const NODE_ENV = getEnv('NODE_ENV', 'production');
export const PORT = getEnv.int('PORT', 3001);
export const JWT_SECRET_KEY = getEnv.string('JWT_SECRET_KEY', '^xUoS$6pS8');
export const couchDb = {

  username: getEnv('COUCHDB_USERNAME', ''),
  password: getEnv('COUCHDB_PASSWORD', ''),
  url: getEnv('COUCHDB_URL', 'http://127.0.0.1'),
  port: getEnv.int('COUCHDB_PORT', 5984),
  bucket: getEnv('COUCHDB_BUCKET', 'go-cars')

};

export const Tdispatch = {
  url: getEnv('TDISPATCH_URL', 'http://localhost:3002')
};

export const defaultCustomer = {
  email: '{fleetName}@gocaller.co.uk',
  lastName: 'gocaller'
};

export const VOICE_API = getEnv.string('VOICE_API_URL', 'http://localhost:3000');
export const AUTH_COOKIE_NAME = 'jwt';
export const AUTH_COOKIE_TTL = getEnv.int('AUTH_COOKIE_TTL', 8.64e+7); // miliseconds
export const CACHE_TTL = getEnv.int('CACHE_TTL', 86400); // seconds
export const CALL_CACHE_TTL = getEnv.int('CALL_CACHE_TTL', 14000);
export const JWT_TOKEN_EXPIRY = getEnv.string('JWT_TOKEN_EXPIRY', '1d');
export const VOICE_PASSWORD = getEnv.string('VOICE_PASSWORD_HASH', '$2a$10$JNAiKIPc6QLV3N4cDJ5uvuJzxggtzjntep6eOoYZqnTiHz.WuB/T2');
export const TWILIO_AUTH_KEY = getEnv.string('TWILIO_AUTH_KEY');
