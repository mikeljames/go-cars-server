import session from './services/session';

export const validateToken = (decodedToken, request, cb) => {
  session.get(decodedToken.sessionId)
  .then(sess => {
    if (!sess) {
      console.log('unauth', decodedToken.sessionId, 'path', request.path);
      return cb(null, false);
    }
    cb(null, true);
  })
  .catch(err => {
    cb(err, false);
  });
};
