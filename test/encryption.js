import {expect} from 'chai';
import {encrypt, decrypt} from '../src/services/td-webhook';
import camelCaseKeys from 'camelcase-keys-recursive';

describe('encrypt decrypt webhook payloads', () => {

  it('should encrypt payload and be decrypted', () => {
    const data = {
      booking_pk: '123',
      new_booking_status: 'incoming',
      old_booking_status: 'draft',
      passenger_pk: '123456789112345678912345'
    };

    const secret = 'PAay6DlzGN9TyFiS';

    const encrypted = encrypt(JSON.stringify(data), secret);
    console.log('encrypted', encrypted);
    const decrypted = decrypt(encrypted, secret);
    console.log('decrypted', decrypted);
    expect(decrypted).to.eql(camelCaseKeys(data));
  });

});
