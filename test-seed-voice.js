'use strict';
/* eslint no-shadow:0*/
const rp = require('request-promise');
const HOST = process.env.HOST;
const redis = require('redis');

const client = redis.createClient();
const clearRedis = () => {
  return new Promise(resolve => {
    client.flushall(resolve);
  });
};
const createAccount = (email = 'm2@mikejam.es.com', fleetName = 'MikesCabs2') =>
  rp.post({
    uri: `${HOST}/fleet/user`,
    body: {
      'email': email,
      'fleetName': fleetName,
      'password': 'test1234',
      'name': 'Mike James'
    },
    json: true
  });

const login = () => {
  return createAccount()
  .then(() => {
    console.log('created user');

    return rp.post(`${HOST}/fleet/user/login`, {
      body: JSON.stringify({
        'username': 'm2@mikejam.es.com',
        'password': 'test1234'
      }),
      headers: {'Content-Type': 'application/json'},
      resolveWithFullResponse: true
    });
  })
  .then(res => {
    const cookie = res.headers['set-cookie'][0].split(';')[0];
    console.log('logged in');
    return rp.post({
      uri: `${HOST}/fleet`,
      body: JSON.stringify({
        'key': 'a65dc6e99d4e012e28b4de65da106cdb',
        'clientId': 'NggWI2w9pz@tdispatch.com',
        'secret': 'AJwyZrek5XuTKKb38ULv2JaVDGbVrCFG',
        'fleetName': 'MikesCabs2',
        'twilio': {
          'accountSid': '12312313132123'
        },
        language: 'en-GB'
      }),
      headers: {'Content-Type': 'application/json', cookie},
      resolveWithFullResponse: true
    });
  })
  .then(res => {
    console.log('fleet created');
    const cookie = res.request.headers.cookie;
    const id = JSON.parse(res.body).id;
    return rp.put({
      uri: `${HOST}/fleet/${id}`,
      body: {
        'key': '165dc6e99d4e012e28b4de65da106cdb',
        'clientId': 'asdadsas@tdispatch.com',
        'secret': '1JwyZrek5XuTKKb38ULv2JaVDGbVrCFG',
        'twilio': {
          'accountSid': '112312313132123'
        },
        'voice': {
          'greeting': 'Hi Thanks for calling MikesCabs2',
          'callback': 'Hi this is MikesCabs2 calling',
          'notRegistered': 'The number you are calling from is not registered. to register this number please visit mikescabs2.com'
        },
        language: 'en-GB'
      },
      json: true,
      headers: {cookie}
    })
    .then(() => ({fleetId: id, cookie}));
  });
};

const createLocation = (fleetId, cookie, name, tel) => {
  return rp.post({
    uri: `${HOST}/fleets/${fleetId}/location`,
    body: {
      name,
      address: '221d Baker Street',
      postcode: 'NW16XE',
      coords: {
        lat: 51.5237445,
        lng: -0.1606577
      },
      telephoneNumber: tel,
      extraInfo: 'pull up outside, beep the horn 3 times and turn the radio onto radio 4, I\'ll be down in a jiffy'
    },
    headers: {cookie},
    json: true
  })
  .then((body) => {
    console.log('created location', body.id);
    return rp.patch({
      uri: `${HOST}/location/${body.id}`,
      body: {oneTimeCode: '1234'},
      json: true
    });
  });
};

clearRedis()
.then(login)
.then((res) => {
  const fleetId = res.fleetId;
  const cookie = res.cookie;

  return Promise.all([
    createLocation(fleetId, cookie, 'Voice 1', '+441934440112'),
    createLocation(fleetId, cookie, 'Voice 2', '+441934440113')
  ]);
})
.then((a) => console.log(`added voice ${a.length} locations`))
.then(() => createAccount('m3@mikejam.es.com', 'MikesCabs1'))
.then(() => process.exit(0))
.catch(e => {
  console.log('ERROR: ', e);
  process.exit(1);
});
