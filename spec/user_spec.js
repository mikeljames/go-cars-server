'use strict';
/* eslint no-shadow:0*/
const frisby = require('frisby');
const HOST = process.env.HOST;
frisby.create('Get Fleet getFleetForPublicConsumption')
.get(`${HOST}/fleets/31f6d40c-cc92-494f-9a3f-e8c38670b492`)
.expectStatus(404) // ensure not 500 not 200
.toss();

frisby.create('New Fleet User')
  .post(`${HOST}/fleet/user`, {
    'email': 'm@mikejam.es.com',
    'fleetName': 'Mikes Cabs',
    'password': 'test1234',
    'name': 'Mike James'
  })
  .expectStatus(200)
  .after(() => {
    frisby.create('Conflict Fleet User')
      .post(`${HOST}/fleet/user`, {
        'email': 'm@mikejam.es.com',
        'fleetName': 'Mikes Cabs',
        'password': 'test1234',
        'name': 'Mike James'
      })
      .expectStatus(409)
    .toss();

    frisby.create('Successful Fleet User Login')
      .post(`${HOST}/fleet/user/login`, {
        'username': 'm@mikejam.es.com',
        'password': 'test1234'
      })
      .expectStatus(200)
      .after((err, res) => {
        const cookie = res.headers['set-cookie'][0].split(';')[0];

        frisby.create('Get My Account')
        .addHeader('Cookie', cookie)
        .get(`${HOST}/fleet/user`)
        .expectStatus(200)
        .expectJSON({
          //  "_id":"9ea36dd2-649e-4a3d-9d94-75bcb80e936e",
          //  "_rev":"1-1585370c9da05160c3b9a0e89fd289d3",
          'name': 'Mike James',
          'fleetName': 'Mikes Cabs',
          'email': 'm@mikejam.es.com',
          'role': 'fleet-admin',
          'type': 'User',
          'fleet': null,
          'fleetId': null
        })
        .toss();

        frisby.create('Successful Fleet Create')
          .addHeader('Cookie', cookie)
          .post(`${HOST}/fleet`, {
            'key': 'a65dc6e99d4e012e28b4de65da106cdb',
            'clientId': 'NggWI2w9pz@tdispatch.com',
            'secret': 'AJwyZrek5XuTKKb38ULv2JaVDGbVrCFG',
            'fleetName': 'Mikes Cabs',
            'twilio': {
              'accountSid': '12312313132123',
              'authToken': '123123131312312312312312',
              'number': '+44193413595',
              'voice': 'man'
            },
            language: 'en-GB'
          }, {json: true})
          .expectStatus(200)
          .after((errJ, resJ, body) => {
            const id = body.id;

            frisby.create('Successful Fleet Update')
              .addHeader('Cookie', cookie)
              .put(`${HOST}/fleet/${id}`, {
                'key': '165dc6e99d4e012e28b4de65da106cdb',
                'clientId': 'asdadsas@tdispatch.com',
                'secret': '1JwyZrek5XuTKKb38ULv2JaVDGbVrCFG',
                'twilio': {
                  'accountSid': '112312313132123',
                  'authToken': '1123123131312312312312312',
                  'number': '+441934413597',
                  'voice': 'man'
                },
                'voice': {
                  'greeting': 'Hi Thanks for calling Mikes Cabs',
                  'callback': 'Hi this is Mikes Cabs calling',
                  'notRegistered': 'The number you are calling from is not registered. to register this number please visit mikescabs.com'
                },
                language: 'en-GB'
              }, {json: true})
              .expectStatus(200)
              .after(() => {

                frisby.create('Get Fleet getFleetForPublicConsumption')
                .get(`${HOST}/fleets/${id}`)
                .expectStatus(200)
                .expectJSON({
                  fleetName: 'Mikes Cabs',
                  number: '+441934413597',
                  language: 'en-GB',
                  voice: 'man'
                })
                .toss();

                frisby.create('Get Fleet by called for voice')
                .addHeader('Authorization', 'Basic dm9pY2U6czJJI20hVEIqTTZIMF4=')
                .get(`${HOST}/fleet/called/+441934413597`)
                .expectJSON({
                  'key': '165dc6e99d4e012e28b4de65da106cdb'
                })
                .expectStatus(200)
                // .after((err, res, body) => {
                //   console.log('CALLED +441934413597', body);
                // })
                .toss();
              })
              .toss();

            frisby.create('New Location')
              .post(`${HOST}/fleets/${id}/location`, {
                name: 'Moriarty',
                address: '221d Baker Street',
                postcode: 'NW16XE',
                coords: {
                  lat: 51.5237445,
                  lng: -0.1606577
                },
                telephoneNumber: '+44207224367',
                extraInfo: '' // 'pull up outside, beep the horn 3 times and turn the radio onto radio 4, I\'ll be down in a jiffy'
              }, {json: true})
              .expectStatus(200)
              .after((err, res, body) => {
                frisby.create('Location verify')
                .patch(`${HOST}/location/${body.id}`, {
                  oneTimeCode: '1234'
                }, {json: true})
                .expectStatus(200)
                .toss();
              })
              .toss();

            frisby.create('New Location')
              .post(`${HOST}/fleets/${id}/location`, {
                name: 'Sherlock Holmes',
                address: '221b Baker Street',
                postcode: 'NW16XE',
                coords: {
                  lat: 51.5237455,
                  lng: -0.1606377
                },
                telephoneNumber: '+44207224368',
                extraInfo: 'pull up outside, beep the horn 3 times and turn the radio onto radio 4, I\'ll be down in a jiffy'
              }, {json: true})
              .expectStatus(200)
              .after((err, res, body) => {
                const shortId = body.shortId;
                frisby.create('Location verify')
                .patch(`${HOST}/location/${body.id}`, {
                  oneTimeCode: '1234'
                }, {json: true})
                .expectStatus(200)
                .after(() => {

                  frisby.create('Book Taxi')
                  .post(`${HOST}/book/${shortId}`, {
                    passengerName: 'Mike',
                    pickUpTime: new Date(),
                    passengerCount: 2
                  }, {json: true})
                  .addHeader('Authorization', 'Basic dm9pY2U6czJJI20hVEIqTTZIMF4=')
                  .expectStatus(200)
                  .after(() => {
                    frisby.create('Confirm booking status')
                    .get(`${HOST}/location/telephone/+44207224368`)
                    .addHeader('Authorization', 'Basic dm9pY2U6czJJI20hVEIqTTZIMF4=')
                    .expectStatus(200)
                    .expectJSON({
                      bookings: (val) => { expect(val.length).toEqual(1); } // Custom matcher callback
                    })
                    .toss();
                  })
                  .toss();

                  frisby.create('Book Taxi')
                  .post(`${HOST}/book/${shortId}`, {
                    passengerName: 'Mike',
                    pickUpTime: new Date(),
                    passengerCount: 2,
                    telephoneNumber: '+447927194681'
                  }, {json: true})
                  .addHeader('Authorization', 'Basic dm9pY2U6czJJI20hVEIqTTZIMF4=')
                  .expectStatus(200)
                  .after(() => {
                    // this test is for when someone books at a location but can move afterwards,
                    // we go by their existing bookings. mobile users or poster ids
                    frisby.create('Confirm booking status')
                    .get(`${HOST}/location/telephone/+447927194681`)
                    .addHeader('Authorization', 'Basic dm9pY2U6czJJI20hVEIqTTZIMF4=')
                    .expectStatus(200)
                    .after((err, res, body) => {
                      const bookings = JSON.parse(body).bookings;
                      const pk = bookings[0].pk;
                      frisby.create('Cancel Booking')
                      .post(`${HOST}/booking/${pk}/cancel`, {
                        description: 'Test description',
                        telephoneNumber: '+447927194681'
                      }, {json: true})
                      .addHeader('Authorization', 'Basic dm9pY2U6czJJI20hVEIqTTZIMF4=')
                      .expectStatus(200)
                      .after(() => {
                        frisby.create('Confirm booking status empty after delete booking')
                        .get(`${HOST}/location/telephone/+447927194681`)
                        .addHeader('Authorization', 'Basic dm9pY2U6czJJI20hVEIqTTZIMF4=')
                        .expectJSON({
                          bookings: []
                        })
                        .expectStatus(200)
                        .toss();

                        frisby.create('Book Taxi 1')
                        .post(`${HOST}/book/${shortId}`, {
                          passengerName: 'Mike',
                          pickUpTime: new Date(),
                          passengerCount: 2
                        }, {json: true})
                        .addHeader('Authorization', 'Basic dm9pY2U6czJJI20hVEIqTTZIMF4=')
                        .expectStatus(200)
                        .after(() => {
                          frisby.create('Book Taxi 2')
                          .post(`${HOST}/book/${shortId}`, {
                            passengerName: 'MikeJTESTFAIL',
                            pickUpTime: new Date(),
                            passengerCount: 2
                          }, {json: true})
                          .addHeader('Authorization', 'Basic dm9pY2U6czJJI20hVEIqTTZIMF4=')
                          .expectStatus(200)
                          .after((err, res, body) => {
                            const pk = body.pk;
                            frisby.create('Cancel Booking Taxi 2')
                            .post(`${HOST}/booking/${pk}/cancel`, {
                              description: 'Test description',
                              telephoneNumber: '+44207224368'
                            }, {json: true})
                            .addHeader('Authorization', 'Basic dm9pY2U6czJJI20hVEIqTTZIMF4=')
                            .expectStatus(200)
                            .after(() => {
                              frisby.create('Confirm booking status empty after delete booking')
                              .get(`${HOST}/location/telephone/+44207224368`)
                              .addHeader('Authorization', 'Basic dm9pY2U6czJJI20hVEIqTTZIMF4=')
                              .expectJSON({
                                bookings: (val) => expect(val.length).toEqual(2)
                              })
                              .expectStatus(200)
                              .toss();


                              frisby.create('Get My Account')
                              .addHeader('Cookie', cookie)
                              .get(`${HOST}/fleet/user`)
                              .expectStatus(200)
                              .expectJSON({
                                'name': 'Mike James',
                                'fleetName': 'Mikes Cabs',
                                'email': 'm@mikejam.es.com',
                                'role': 'fleet-admin',
                                'type': 'User',
                                'fleetId': id,
                                'fleetActivated': true,
                                'fleet': {
                                  'key': '165dc6e99d4e012e28b4de65da106cdb',
                                  'clientId': 'asdadsas@tdispatch.com',
                                  'secret': '1JwyZrek5XuTKKb38ULv2JaVDGbVrCFG',
                                  'fleetName': 'Mikes Cabs',
                                  'twilio': {
                                    'accountSid': '112312313132123',
                                    'authToken': '1123123131312312312312312',
                                    'number': '+441934413597'
                                  },
                                  language: 'en-GB',
                                  'appEnabled': false,
                                  'website': null,
                                  'defaultCustomer': {
                                    'email': 'MikesCabs@gocaller.co.uk',
                                    'firstName': 'Mikes Cabs',
                                    'lastName': 'gocaller'
                                  },
                                  'type': 'Fleet',
                                  '_id': id
                                }
                              })
                              .toss();

                              frisby.create('See all locations for fleet 1')
                              .get(`${HOST}/fleet/locations`)
                              .addHeader('Cookie', cookie)
                              .expectStatus(200)
                              .after((err, res, body) => {
                                const locationId = JSON.parse(body)[0].id;
                                console.log(JSON.parse(body));

                                frisby.create('Update location from fleet account')
                                .addHeader('Cookie', cookie)
                                .put(`${HOST}/fleet/locations/${locationId}`, {
                                  name: 'Sherlock Holmes v2'
                                })
                                .expectStatus(200)
                                .after(() => {

                                  frisby.create('See all locations for fleet 1')
                                  .get(`${HOST}/fleet/locations`)
                                  .addHeader('Cookie', cookie)
                                  .expectStatus(200)
                                  .expectJSON('0', {
                                    name: 'Sherlock Holmes v2'
                                  })
                                  .after((err, res, body) => {
                                    const id = JSON.parse(body)[0].id;
                                    frisby.create(`Delete Location ${id}`)
                                    .delete(`${HOST}/fleet/locations/${id}`)
                                    .addHeader('Cookie', cookie)
                                    .expectStatus(200)
                                    .after(() => {
                                      frisby.create('Successful Logout')
                                      .post(`${HOST}/fleet/user/logout`)
                                      .addHeader('Cookie', cookie)
                                      .expectJSON({
                                        message: 'You\'ve been successfully logged out'
                                      })
                                      .after(() => {
                                        frisby.create('Get My Account Fails')
                                        .addHeader('Cookie', cookie)
                                        .get(`${HOST}/fleet/user`)
                                        .expectStatus(401)
                                        .toss();
                                      })
                                      .toss();
                                    })
                                    .toss();
                                  })
                                  .toss();
                                })
                                .toss();
                              })
                              .toss();
                            })
                            .toss();
                          })
                          .toss();
                        })
                        .toss();
                      })
                      .toss();
                    })
                    .toss();
                  })
                  .toss();
                })
                .toss();
              })
              .toss();
          })
          .toss();
      })
      .toss();
  })
  .toss();

frisby.create('Unauthorised Fleet User Login')
  .post(`${HOST}/fleet/user/login`, {
    'username': 'm@mikejam.com',
    'password': 'test1234'
  })
  .expectStatus(401)
  .toss();

frisby.create('Forgot Password Request')
  .post(`${HOST}/fleet/user/forgot`, {
    'username': 'm@mikejam.es.com'
  })
  .expectStatus(200)
  .after(() => {
    // no parameters returned 'this is in the email'

    frisby.create('Update Password')
    .put(`${HOST}/fleet/user/password/1a5ae654-6388-47f4-b7b4-641f01bfb248`, {
      'password': 'test12345'
    })
    .expectStatus(200) // update to 201
    .after(() => {
      frisby.create('Successful Fleet User Login with new password')
      .post(`${HOST}/fleet/user/login`, {
        'username': 'm@mikejam.es.com',
        'password': 'test12345'
      })
      .expectStatus(200)
      .toss();

      frisby.create('Failed Fleet User Login with new password')
      .post(`${HOST}/fleet/user/login`, {
        'username': 'm@mikejam.es.com',
        'password': 'test1234'
      })
      .expectStatus(401)
      .toss();

    })
    .toss();
  })
  .toss();
