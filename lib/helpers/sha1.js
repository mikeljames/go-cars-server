'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var crypto = require('crypto');

exports.default = function (k) {
  var shasum = crypto.createHash('sha1');
  shasum.update(k);
  return shasum.digest('hex');
};