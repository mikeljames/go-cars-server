'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _boom = require('boom');

var _boom2 = _interopRequireDefault(_boom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (roleExpected, handler) {
  return function (request, reply) {
    var role = request.auth.credentials.user.role;


    if (role === roleExpected) {
      return handler(request, reply);
    }

    reply(_boom2.default.notFound());
  };
};