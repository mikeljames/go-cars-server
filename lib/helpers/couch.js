'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.couchDesignDoc = exports.couchView = exports.couchDelete = exports.couchUpdate = exports.couchTypeGet = exports.couchGet = exports.couchSave = undefined;

var _cradle = require('cradle');

var _cradle2 = _interopRequireDefault(_cradle);

var _config = require('../config');

var Config = _interopRequireWildcard(_config);

var _bluebird = require('bluebird');

var _nodeUuid = require('node-uuid');

var _nodeUuid2 = _interopRequireDefault(_nodeUuid);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var db = void 0;
if (process.env.NODE_ENV === 'development') {

  db = new _cradle2.default.Connection();
} else {

  db = new _cradle2.default.Connection(Config.couchDb.url, Config.couchDb.port, {
    auth: {
      username: Config.couchDb.username,
      password: Config.couchDb.password
    }
  });
}

db = db.database(Config.couchDb.bucket);

exports.default = db;
var couchSave = exports.couchSave = function couchSave() {
  var id = arguments.length <= 0 || arguments[0] === undefined ? _nodeUuid2.default.v4() : arguments[0];
  var doc = arguments[1];

  return (0, _bluebird.promisify)(db.save.bind(db))(id, doc);
};

var couchGet = exports.couchGet = (0, _bluebird.promisify)(db.get.bind(db));
var couchTypeGet = exports.couchTypeGet = function couchTypeGet(id, type) {
  return couchGet(id).then(function (d) {
    return d.type === type ? d : null;
  });
};
var couchUpdate = exports.couchUpdate = (0, _bluebird.promisify)(db.merge.bind(db));
var couchDelete = exports.couchDelete = (0, _bluebird.promisify)(db.remove.bind(db));

var couchView = exports.couchView = function couchView(docId, queryOptions) {
  var optionalOpts = arguments.length <= 2 || arguments[2] === undefined ? { value: false } : arguments[2];

  var promise = (0, _bluebird.promisify)(db.view.bind(db)).call(db, docId, queryOptions);
  if (optionalOpts.value) {
    return promise.then(function (res) {
      return res.map(function (r) {
        return Object.assign({}, r, { id: r._id });
      });
    });
  }

  return promise;
};
var couchDesignDoc = exports.couchDesignDoc = function couchDesignDoc(name, indexes) {
  return couchSave('_design/' + name, indexes);
};

// TODO couchDesignDoc abstraction returns an object containing the generated couchView methods,
// which can then be exposed in a module