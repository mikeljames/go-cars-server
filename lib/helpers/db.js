'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var Cradle = require('cradle'),
    Config = require('../config');

var internals = {};

module.exports = internals;

internals.createDbConnection = function createDbConnection() {

	var db;

	if (process.env.NODE_ENV === 'development') {

		db = new Cradle.Connection();
	} else {

		db = new Cradle.Connection(Config.couchDb.url, Config.couchDb.port, {
			auth: {
				username: Config.couchDb.username,
				password: Config.couchDb.password
			}
		});
	}

	db.status = function (callback) {
		return function (err, result) {
			if (err) {
				if ((typeof err === 'undefined' ? 'undefined' : _typeof(err)) === 'object' && err.hasOwnProperty('error') && err.error === 'not_found') {
					err.notFound = true;
					return callback(err, result);
				}

				callback(err, result);
			} else {
				callback(err, result);
			}
		};
	};

	db.connectToBucket = function (bucketName) {
		var deferred = Q.defer();
		var _this = this;
		internals.exists(db, bucketName).then(internals.createOrReturnBucket(db, bucketName)).then(function (connectionToBucket) {
			_this.connectionToBucket = connectionToBucket;
			deferred.resolve(db);
		}).catch(function (err) {
			console.log('failed to create bucket');

			deferred.reject(err);
		});

		return deferred.promise;
	};

	return db;
};

internals.exists = function (db, bucketName) {
	return function (callback) {
		var deferred = Q.defer();

		db.exists(bucketName, function (err, exists) {
			if (err) {
				deferred.reject(err);
			} else {
				deferred.resolve(exists);
			}
		});

		return deferred.promise;
	};
};

internals.createOrReturnBucket = function (db, bucketName) {

	return function (exists) {
		var deferred = Q.defer();

		if (exists) {

			db.database(bucketName);
			deferred.resolve(db);
		} else {

			db.database(bucketName);

			db.create(function (err) {
				if (err) {
					console.log('Failed to create bucket %s', bucketName);
					deferred.reject(err);
				} else {
					deferred.resolve(db);
				}
			});
		}

		return deferred.promise;
	};
};