'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateToken = undefined;

var _session = require('./services/session');

var _session2 = _interopRequireDefault(_session);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var validateToken = exports.validateToken = function validateToken(decodedToken, request, cb) {
  _session2.default.get(decodedToken.sessionId).then(function (sess) {
    if (!sess) {
      console.log('unauth', decodedToken.sessionId, 'path', request.path);
      return cb(null, false);
    }
    cb(null, true);
  }).catch(function (err) {
    cb(err, false);
  });
};