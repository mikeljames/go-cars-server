'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redis = require('redis');

var _redis2 = _interopRequireDefault(_redis);

var _config = require('../config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var client = void 0;

if (process.env.NODE_ENV === 'production') {
  client = _redis2.default.createClient(process.env.REDIS_URL);
} else {
  client = _redis2.default.createClient();
}

function set(key, data) {
  var ttl = arguments.length <= 2 || arguments[2] === undefined ? _config.CACHE_TTL : arguments[2];

  return new Promise(function (resolve, reject) {
    // console.log('set', key, data);
    client.set(key, JSON.stringify(data), function (err) {
      if (err) return reject(err);

      resolve(key);

      client.expire(key, ttl);
    });
  });
}

function get(key) {
  return new Promise(function (resolve, reject) {
    client.get(key, function (err, res) {
      if (err) return reject(err);
      try {
        var parsed = JSON.parse(res);
        resolve(parsed);
      } catch (e) {
        console.log(e);
        resolve({});
      }
    });
  });
}

function del(key) {
  client.del(key);
  return Promise.resolve();
}

exports.default = {
  set: set,
  get: get,
  del: del
};