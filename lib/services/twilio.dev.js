'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.numbers = exports.account = exports.releaseNumber = exports.purchaseNumber = exports.searchNumber = undefined;

var _config = require('../config');

var _twilio = require('twilio');

var _twilio2 = _interopRequireDefault(_twilio);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

console.log('TWILIO DEV MODE IS ENABLED');
var searchNumber = exports.searchNumber = function searchNumber(accountSid, isoCode, number) {
  return (0, _twilio2.default)(accountSid, _config.TWILIO_AUTH_KEY).availablePhoneNumbers(isoCode).local.list({ contains: number }).then(function (data) {
    return data.availablePhoneNumbers;
  }).catch(function () {
    return [];
  });
};

var random = function random() {
  return Math.floor(Math.random() * 10);
};
var purchased = [{
  phoneNumber: '+528153511646',
  sid: '1231231243hj1h23jh1231231'
}
// {
//   phoneNumber: '+4419344154232',
//   sid: '1231231243hj1h23jh4531231'
// },
// {
//   phoneNumber: '+4419344154872',
//   sid: '1231231243hj1h23jh4538731'
// }
];

var purchaseNumber = exports.purchaseNumber = function purchaseNumber(_ref) {
  var phoneNumber = _ref.phoneNumber;
  return Promise.resolve({
    sid: '1231231243hj1h23jh123' + random(),
    phoneNumber: phoneNumber
  }).then(function (n) {
    purchased.push(n);
    return n;
  });
};

var releaseNumber = exports.releaseNumber = function releaseNumber() {
  return Promise.resolve();
};

var account = exports.account = function account(accountSid) {
  return (0, _twilio2.default)(accountSid, _config.TWILIO_AUTH_KEY).accounts(accountSid).get();
};

var numbers = exports.numbers = function numbers() {
  return Promise.resolve({
    incomingPhoneNumbers: purchased
  });
};