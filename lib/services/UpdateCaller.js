'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _twilio = require('twilio');

var _twilio2 = _interopRequireDefault(_twilio);

var _session = require('./session');

var _session2 = _interopRequireDefault(_session);

var _config = require('../config');

var _tdispatchContribBookingTypes = require('tdispatch-contrib-booking-types');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function bookingDriverExistsAndPreviousStatusNoDriver(booking, bookingStoredLocal) {
  return booking.driverPk && !bookingStoredLocal.driverPk;
}

function previousAndNewHaveDrivers(booking, bookingStoredLocal) {
  return booking.driverPk && bookingStoredLocal.driverPk;
}

function driversAreNotTheSame(booking, bookingStoredLocal) {
  return bookingStoredLocal.driverPk.id !== booking.driverPk.id;
}

function previousAndNextStatusAreDifferent(booking, bookingStoredLocal) {
  return bookingStoredLocal.status !== booking.status;
}

function isPhoneCallStatus(booking) {
  return booking.status === _tdispatchContribBookingTypes.types.ARRIVED_WAITING || booking.status === _tdispatchContribBookingTypes.types.ON_THE_WAY || booking.status === _tdispatchContribBookingTypes.types.MISSED;
}

function updateStoredLocalStatus(bookingStoredLocal, booking, tel, bookings) {
  bookingStoredLocal.status = booking.status; // pass by reference yuk!
  return _session2.default.set(tel, bookings);
}

function removeEndOfSessionBookings(bookings) {
  return bookings.filter(function (b) {
    return b.status !== _tdispatchContribBookingTypes.types.CANCELLED || b.status !== _tdispatchContribBookingTypes.types.COMPLETED;
  });
}

exports.default = function (fleet, _ref) {
  var booking = _ref.booking;


  return new Promise(function (resolve, reject) {
    var tel = booking.passengerPhone.replace('+', '');
    return _session2.default.get(tel).then(function (bookings) {

      if (!bookings) {
        resolve();
      }
      console.log('bookings.find', bookings);
      console.log('typeof bookings', bookings);

      var bookingStoredLocal = bookings.find(function (b) {
        return b.key === booking.key;
      });

      if (!bookingStoredLocal) {
        return resolve();
      }
      console.log('passengerPhone', booking.passengerPhone, 'status', booking.status);
      console.log('bookingStoredLocal.status', bookingStoredLocal.status);
      console.log('booking.status', booking.status);

      // if bookingLocal.driver.
      if (isPhoneCallStatus(booking)) {
        console.log('is phone call status');

        if (bookingDriverExistsAndPreviousStatusNoDriver(booking, bookingStoredLocal) || previousAndNewHaveDrivers(booking, bookingStoredLocal) && driversAreNotTheSame(booking, bookingStoredLocal)) {
          console.log('bookings have different drivers');

          if (previousAndNextStatusAreDifferent(booking, bookingStoredLocal)) {
            console.log('making phone call');

            return makePhoneCall(fleet, booking).then(function () {
              return updateStoredLocalStatus(bookingStoredLocal, booking, tel, bookings);
            });
          }
        }
      }

      return updateStoredLocalStatus(bookingStoredLocal, booking, tel, removeEndOfSessionBookings(bookings));
    }).catch(function (e) {
      console.log('Failed to fetch bookings for ' + booking.passengerPhone);
      reject(e);
    });
  });
};

function makePhoneCall(fleet, booking) {
  var twilioCredentials = fleet.twilio;
  var passengers = booking.passengers;
  var status = booking.status;
  var pickupTime = _moment2.default.utc(booking.pickupTime).format();
  var pk = booking.pk;

  return new Promise(function (resolve, reject) {
    console.log('twilioCredentials', twilioCredentials);
    var client = (0, _twilio2.default)(twilioCredentials.accountSid, fleet.twilioConnect ? _config.TWILIO_AUTH_KEY : twilioCredentials.authToken);
    var callbackUrl = _config.VOICE_API + '/status/callback?status=' + status + '&passengers=' + passengers + '&pickupTime=' + pickupTime + '&pk=' + pk;
    console.log('payload', callbackUrl, 'to ', booking.passengerPhone, 'from', twilioCredentials.number);
    client.calls.create({
      url: callbackUrl,
      to: booking.passengerPhone,
      from: twilioCredentials.number,
      method: 'POST'
    }, function (err) {

      if (err) {
        return reject(err);
      }

      resolve();
    });
  });
}