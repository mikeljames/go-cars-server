'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var apiUrl = require('../config').Tdispatch.url;
var resolveUrl = require('url').resolve;
var bodyParser = require('./RequestBodyParser');
var curry = require('curry');
var getAccessToken = require('./Auth').getAccessToken;
var request = require('request');

var get = curry(function (bookingHookPayload, accessToken) {

  var url = resolveUrl(apiUrl, '/passenger/v1/bookings/' + bookingHookPayload.booking_pk + '?access_token=' + accessToken.access_token);

  return new Promise(function (resolve, reject) {
    request.get(url, bodyParser(resolve, reject));
  });
});

var track = curry(function (bookingHookPayload, accessToken) {
  var url = resolveUrl(apiUrl, '/passenger/v1/bookings/track?access_token=' + accessToken.access_token);

  return new Promise(function (resolve, reject) {
    request.post({
      url: url,
      headers: {
        'Content-Type': 'application/json'
      },
      timeout: 20000,
      body: JSON.stringify({
        booking_pks: [bookingHookPayload.booking_pk]
      })
    }, bodyParser(resolve, reject));
  });
});

var getBooking = exports.getBooking = curry(function (fleet, bookingHookPayload) {
  var accessToken = getAccessToken({
    username: fleet.fleetCustomer.email,
    password: fleet.fleetCustomer.password,
    secret: fleet.secret,
    key: fleet.key,
    clientId: fleet.clientId
  });

  var getResult = accessToken.then(get(bookingHookPayload));

  var trackResult = accessToken.then(track(bookingHookPayload));

  return Promise.all([getResult, trackResult]).then(function (bookings) {
    return {
      booking: bookings[0],
      bookingStatus: bookings[1].bookings[0],
      fleet: fleet
    };
  });
});