'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.create = undefined;

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _camelcaseKeysRecursive = require('camelcase-keys-recursive');

var _camelcaseKeysRecursive2 = _interopRequireDefault(_camelcaseKeysRecursive);

var _config = require('../config');

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var create = exports.create = function create(_ref, customer) {
  var key = _ref.key;
  var clientId = _ref.clientId;


  var url = _url2.default.resolve(_config.Tdispatch.url, '/passenger/v1/accounts?key=' + key + '&client_id=' + clientId);

  return _requestPromise2.default.post({
    url: url,
    body: {
      first_name: customer.firstName,
      last_name: customer.lastName,
      email: customer.email,
      phone: customer.phone,
      password: customer.password,
      client_id: clientId
    },
    json: true
  }).then(_camelcaseKeysRecursive2.default).catch(function (e) {
    console.log(e.message);
    throw e;
  });
};