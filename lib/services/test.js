'use strict';

var _Email = require('./Email');

var _Email2 = _interopRequireDefault(_Email);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _Email2.default)({
  to: 'm@mikejam.es',
  subject: '🚕  Welcome to gocaller.co.uk! Please confirm your email address',
  html: (0, _Email.activateTemplate)('https://www.gocaller.co.uk')
}).then(function (res) {
  console.log(res);
}).catch(function (err) {
  console.log(err);
});