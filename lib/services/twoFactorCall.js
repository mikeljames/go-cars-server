'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _twilio = require('twilio');

var _twilio2 = _interopRequireDefault(_twilio);

var _config = require('../config');

var _session = require('./session');

var _session2 = _interopRequireDefault(_session);

var _randomString = require('random-string');

var _randomString2 = _interopRequireDefault(_randomString);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isTestMode = process.env.TEST_MODE === 'true';

exports.default = function (fleet, location) {
  var oneTimeCode = isTestMode ? '1234' : (0, _randomString2.default)({
    length: 4,
    numeric: true,
    letters: false,
    special: false
  });

  var _fleet$twilio = fleet.twilio;
  var accountSid = _fleet$twilio.accountSid;
  var authToken = _fleet$twilio.authToken;
  var number = _fleet$twilio.number;

  // console.log('setting oneTimeCode', `${oneTimeCode}`);

  return _session2.default.set(location.id + '-' + oneTimeCode, location.id, 600).then(function () {
    // ensure we can still make calls for non twilio connect users
    return (0, _twilio2.default)(accountSid, fleet.twilioConnect ? _config.TWILIO_AUTH_KEY : authToken).calls.create({
      url: _config.VOICE_API + '/location/verify?oneTimeCode=' + oneTimeCode,
      to: location.telephoneNumber,
      from: number,
      method: 'POST'
    });
  });
};