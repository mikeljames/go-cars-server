'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.encrypt = exports.decrypt = undefined;

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _camelcaseKeysRecursive = require('camelcase-keys-recursive');

var _camelcaseKeysRecursive2 = _interopRequireDefault(_camelcaseKeysRecursive);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var decrypt = exports.decrypt = function decrypt(data, secret) {
  return new Promise(function (resolve, reject) {
    var payload = new Buffer(data, 'base64').toString('binary');
    var iv = payload.substr(0, 16);
    var encryptedData = payload.substr(16);

    try {
      var decipher = _crypto2.default.createDecipheriv('aes-128-cbc', secret, iv);

      decipher.setAutoPadding(false);

      var dec = decipher.update(encryptedData, 'binary', 'utf8');

      dec += decipher.final('utf8');
      console.log(dec);
      dec = JSON.parse(dec.toString().replace(/\*/g, ''));

      resolve((0, _camelcaseKeysRecursive2.default)(dec));
    } catch (e) {
      reject(e);
    }
  });
};

var encrypt = exports.encrypt = function encrypt(data, secret) {
  var clearEncoding = 'utf8';
  var cipherEncoding = 'hex';
  var cipher = _crypto2.default.createCipheriv('aes-128-cbc', secret, _crypto2.default.randomBytes(16));
  var cipherChunks = [];
  cipher.setAutoPadding(false);
  cipherChunks.push(cipher.update(data, clearEncoding, cipherEncoding));

  var byteLength = Buffer.byteLength(cipherChunks.join(''), 'hex');
  console.log('byteLength before padding', byteLength);
  // if (byteLength > 128) {
  //   if (byteLength > 128) {

  //     let padding = '';
  //     for (let i = 0; i < (byteLength - 128)/8; i++) {
  //       padding += '*';
  //     }

  //     console.log('padding length', Buffer.byteLength(padding, 'hex'));
  //     cipherChunks.push(padding, clearEncoding, cipherEncoding);

  //   }
  // }

  if (byteLength < 128) {

    var padding = '';

    for (var i = 0; i < 128 - byteLength; i++) {
      padding += '*';
    }
    console.log('padding length', Buffer.byteLength(padding, 'hex'));

    cipherChunks.push(cipher.update(padding, clearEncoding, cipherEncoding));
  }

  console.log('length after padding', Buffer.byteLength(cipherChunks.join(), 'hex'));
  var fin = cipher.final(cipherEncoding);

  console.log('fin length', Buffer.byteLength(fin, 'hex'));

  return fin;
};