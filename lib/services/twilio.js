'use strict';

if (process.env.NODE_ENV === 'development') {
  module.exports = require('./twilio.dev');
} else {
  module.exports = require('./twilio.prod');
}