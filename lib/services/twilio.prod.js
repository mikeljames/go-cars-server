'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.numbers = exports.account = exports.releaseNumber = exports.purchaseNumber = exports.searchNumber = undefined;

var _config = require('../config');

var _twilio = require('twilio');

var _twilio2 = _interopRequireDefault(_twilio);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var searchNumber = exports.searchNumber = function searchNumber(accountSid, isoCode, number) {
  return (0, _twilio2.default)(accountSid, _config.TWILIO_AUTH_KEY).availablePhoneNumbers(isoCode).local.list({ contains: number }).then(function (data) {
    return data.availablePhoneNumbers;
  }).catch(function () {
    return [];
  });
};

var purchaseNumber = exports.purchaseNumber = function purchaseNumber(_ref) {
  var accountSid = _ref.accountSid;
  var friendlyName = _ref.friendlyName;
  var voiceUrl = _ref.voiceUrl;
  var phoneNumber = _ref.phoneNumber;
  return (0, _twilio2.default)(accountSid, _config.TWILIO_AUTH_KEY).incomingPhoneNumbers.create({
    phoneNumber: phoneNumber,
    friendlyName: friendlyName,
    voiceUrl: voiceUrl
  });
};

var releaseNumber = exports.releaseNumber = function releaseNumber(_ref2) {
  var accountSid = _ref2.accountSid;
  var numberSid = _ref2.numberSid;
  return (0, _twilio2.default)(accountSid, _config.TWILIO_AUTH_KEY).incomingPhoneNumbers(numberSid).delete();
};

var account = exports.account = function account(accountSid) {
  return (0, _twilio2.default)(accountSid, _config.TWILIO_AUTH_KEY).accounts(accountSid).get();
};

var numbers = exports.numbers = function numbers(accountSid) {
  return (0, _twilio2.default)(accountSid, _config.TWILIO_AUTH_KEY).incomingPhoneNumbers.list();
};