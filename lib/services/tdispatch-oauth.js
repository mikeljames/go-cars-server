'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _config = require('../config');

var Config = _interopRequireWildcard(_config);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _camelcaseKeysRecursive = require('camelcase-keys-recursive');

var _camelcaseKeysRecursive2 = _interopRequireDefault(_camelcaseKeysRecursive);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TdispatchAuth = function () {
  function TdispatchAuth(opts) {
    _classCallCheck(this, TdispatchAuth);

    this.secret = opts.secret;
    this.key = opts.key;
    this.clientId = opts.clientId;
    this.username = opts.username;
    this.password = opts.password;
  }

  _createClass(TdispatchAuth, [{
    key: 'getAccessToken',
    value: function getAccessToken() {
      var _this = this;

      return this.getAuthCode().then(function (code) {
        return _this.authenticate(code);
      });
    }
  }, {
    key: 'getAuthCode',
    value: function getAuthCode() {
      var AccessCodeUrl = _url2.default.resolve(Config.Tdispatch.url, 'passenger/oauth2/auth?response_type=code&client_id=' + this.clientId + '&key=' + this.key + '&response_format=json&scope=');

      return _requestPromise2.default.post({
        uri: AccessCodeUrl,
        body: {
          username: this.username,
          password: this.password
        },
        json: true
      }).then(_camelcaseKeysRecursive2.default).catch(function (e) {
        console.log('An error occured when gettingAuthCode', e.response);
        console.log('Response body: ', e.response.body);
      });
    }
  }, {
    key: 'authenticate',
    value: function authenticate(_ref) {
      var authCode = _ref.authCode;

      return _requestPromise2.default.post({
        uri: _url2.default.resolve(Config.Tdispatch.url, '/passenger/oauth2/token'),
        body: {
          code: authCode,
          redirect_url: '',
          grant_type: 'authorization_code',
          client_secret: this.secret,
          client_id: this.clientId
        },
        json: true
      }).then(_camelcaseKeysRecursive2.default);
    }
  }]);

  return TdispatchAuth;
}();

exports.default = TdispatchAuth;