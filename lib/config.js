'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TWILIO_AUTH_KEY = exports.VOICE_PASSWORD = exports.JWT_TOKEN_EXPIRY = exports.CALL_CACHE_TTL = exports.CACHE_TTL = exports.AUTH_COOKIE_TTL = exports.AUTH_COOKIE_NAME = exports.VOICE_API = exports.defaultCustomer = exports.Tdispatch = exports.couchDb = exports.JWT_SECRET_KEY = exports.PORT = exports.NODE_ENV = undefined;

var _getenv = require('getenv');

var _getenv2 = _interopRequireDefault(_getenv);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NODE_ENV = exports.NODE_ENV = (0, _getenv2.default)('NODE_ENV', 'production');
var PORT = exports.PORT = _getenv2.default.int('PORT', 3001);
var JWT_SECRET_KEY = exports.JWT_SECRET_KEY = _getenv2.default.string('JWT_SECRET_KEY', '^xUoS$6pS8');
var couchDb = exports.couchDb = {

  username: (0, _getenv2.default)('COUCHDB_USERNAME', ''),
  password: (0, _getenv2.default)('COUCHDB_PASSWORD', ''),
  url: (0, _getenv2.default)('COUCHDB_URL', 'http://127.0.0.1'),
  port: _getenv2.default.int('COUCHDB_PORT', 5984),
  bucket: (0, _getenv2.default)('COUCHDB_BUCKET', 'go-cars')

};

var Tdispatch = exports.Tdispatch = {
  url: (0, _getenv2.default)('TDISPATCH_URL', 'http://localhost:3002')
};

var defaultCustomer = exports.defaultCustomer = {
  email: '{fleetName}@gocaller.co.uk',
  lastName: 'gocaller'
};

var VOICE_API = exports.VOICE_API = _getenv2.default.string('VOICE_API_URL', 'http://localhost:3000');
var AUTH_COOKIE_NAME = exports.AUTH_COOKIE_NAME = 'jwt';
var AUTH_COOKIE_TTL = exports.AUTH_COOKIE_TTL = _getenv2.default.int('AUTH_COOKIE_TTL', 8.64e+7); // miliseconds
var CACHE_TTL = exports.CACHE_TTL = _getenv2.default.int('CACHE_TTL', 86400); // seconds
var CALL_CACHE_TTL = exports.CALL_CACHE_TTL = _getenv2.default.int('CALL_CACHE_TTL', 14000);
var JWT_TOKEN_EXPIRY = exports.JWT_TOKEN_EXPIRY = _getenv2.default.string('JWT_TOKEN_EXPIRY', '1d');
var VOICE_PASSWORD = exports.VOICE_PASSWORD = _getenv2.default.string('VOICE_PASSWORD_HASH', '$2a$10$JNAiKIPc6QLV3N4cDJ5uvuJzxggtzjntep6eOoYZqnTiHz.WuB/T2');
var TWILIO_AUTH_KEY = exports.TWILIO_AUTH_KEY = _getenv2.default.string('TWILIO_AUTH_KEY');