'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateBasic = undefined;

var _config = require('./config');

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var users = {
  voice: {
    name: 'voice_user',
    username: 'voice',
    password: _config.VOICE_PASSWORD
  }
};

var validateBasic = exports.validateBasic = function validateBasic(request, username, password, callback) {

  var user = users[username];
  if (!user) {
    return callback(null, false);
  }

  _bcrypt2.default.compare(password, user.password, function (err, isValid) {
    if (err) {
      console.log('login error', username, password, user.password);
    }

    callback(err, isValid, {
      name: user.name
    });
  });
};