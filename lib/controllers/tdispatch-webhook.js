'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _tdWebhook = require('../services/td-webhook');

var _UpdateCaller = require('../services/UpdateCaller');

var _UpdateCaller2 = _interopRequireDefault(_UpdateCaller);

var _fleet = require('../models/fleet');

var Fleet = _interopRequireWildcard(_fleet);

var _booking = require('../models/booking');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint no-use-before-define:0 */
exports.default = function (request, reply) {
  var encryptedData = request.payload.payload;
  var decryptMode = request.query.decryptMode && process.env.TEST_MODE === 'true' ? false : true;

  var fleetHash = request.payload['fleet-hash'];

  console.log('payload', JSON.stringify(request.payload));

  reply(); // we don't care about response to tdispatch

  Fleet.getFleetByKeyHash(fleetHash).then(function (fleet) {
    var promise = null;

    if (decryptMode) promise = (0, _tdWebhook.decrypt)(encryptedData, fleet.webhookKey);else promise = Promise.resolve(encryptedData);

    return promise
    // we need to request t-dispatch per webhook request to get the full booking record
    // we store bookings based on telephoneNumber in redis
    .then((0, _booking.getBookingFromWebhookPayload)(fleet))
    // pass booking from previous request to updateCaller
    .then(function (booking) {
      if (booking) {
        return (0, _UpdateCaller2.default)(fleet, booking);
      }
    });
    // .catch(e => console.log(`Error on decrypt update caller fleet: ${fleet.id} Error:`, e));
  }).catch(function (e) {
    return console.log('Error getFleet', e);
  });
};