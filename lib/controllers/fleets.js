'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getByCalled = exports.getAll = exports.put = exports.getById = exports.getFleetForPublicConsumption = exports.post = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _fleet = require('../models/fleet');

var Fleet = _interopRequireWildcard(_fleet);

var _user = require('../models/user');

var _boom = require('boom');

var _boom2 = _interopRequireDefault(_boom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var post = exports.post = function post(request, reply) {
  var payload = request.payload;
  var accountId = request.auth.credentials.accountId;


  return Fleet.save(payload).then(function (fleetSaved) {
    return (0, _user.userHasActivatedFleet)(accountId, fleetSaved).then(function () {
      return { id: fleetSaved.id };
    });
  }).then(function (result) {
    delete result.secret;
    reply(result);
  }).catch(function (err) {
    console.log(err);
    reply(_boom2.default.badImplementation('Failed to save new fleet'));
  });
};

var getFleetForPublicConsumption = exports.getFleetForPublicConsumption = function getFleetForPublicConsumption(request, reply) {
  var id = request.params.id;


  Fleet.get(id).then(function (fleet) {
    if (!fleet) {
      return reply(_boom2.default.notFound());
    }

    var omitted = {
      fleetName: fleet.fleetName,
      number: fleet.twilio ? fleet.twilio.number : null,
      language: fleet.language,
      voice: fleet.twilio.voice
    };

    reply(omitted);
  }).catch(function (err) {
    if (err.error === 'not_found') {
      return reply(_boom2.default.notFound('Fleet not found'));
    }
    console.log('Error getting fleet for public consumption', err.stack);
    reply(_boom2.default.badImplementation());
  });
};

var getById = exports.getById = function getById(request, reply) {
  Fleet.get(request.params.id).then(function (fleet) {
    reply(fleet);
  }).catch(function (e) {
    console.log(e.stack);
    reply(_boom2.default.badImplementation());
  });
};

var put = exports.put = function put(request, reply) {
  var id = request.params.id;

  Fleet.get(id).then(function (fleet) {
    return Fleet.update(id, _extends({}, request.payload, {
      // ensure twilio data is not lost as its a nested structure :(
      twilio: _extends({}, fleet.twilio, request.payload.twilio)
    }));
  }).then(function (fleet) {
    return reply(fleet);
  }).catch(function (e) {
    if (e.error === 'not_found') {
      return reply(_boom2.default.badRequest('Fleet Id Not Found'));
    }
    console.log(e);
    reply(_boom2.default.badImplementation(e));
  });
};

var getAll = exports.getAll = function getAll(request, reply) {
  Fleet.getAll().then(reply).catch(function (e) {
    return reply(_boom2.default.badImplementation(e));
  });
};

var getByCalled = exports.getByCalled = function getByCalled(request, reply) {
  var called = request.params.called;

  Fleet.getByCalled(called).then(reply).catch(function (e) {
    if (e.error === 'not_found') {
      return reply();
    }

    console.log('Fleet.getByCalled ERR', e);
    reply(_boom2.default.badImplementation());
  });
};