'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.default = function (request, reply) {
  var _request$payload = request.payload;
  var caller = _request$payload.caller;
  var called = _request$payload.called;
  // called being a 121 number caller being the user making the call
  // use this for the users booking session data

  (0, _location.getByTelephoneNumber)(called, caller).then(function (location) {
    if (!location.name) {
      // called is not a 121 number as it doesnt return a location
      // lookup by caller for location
      // and lookup fleet by called
      return Promise.all([(0, _location.getByTelephoneNumber)(caller), (0, _fleet.getByCalled)(called)]).then(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 2);

        var loc = _ref2[0];
        var fleet = _ref2[1];
        return {
          location: loc,
          fleet: fleet,
          isOneToOne: false
        };
      }).catch(function () {
        return {};
      });
    }
    // caller is making call at a known location {tel: called}
    return (0, _fleet.get)(location.fleetId).then(function (fleet) {
      return {
        location: location,
        fleet: fleet,
        isOneToOne: true
      };
    }).catch(function () {
      return {};
    });
  }).then(function (identifed) {
    return reply(identifed);
  }).catch(function (e) {
    console.log(e.stack);
    reply(_boom2.default.badImplementation());
  });
};

var _location = require('../models/location');

var _fleet = require('../models/fleet');

var _boom = require('boom');

var _boom2 = _interopRequireDefault(_boom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }