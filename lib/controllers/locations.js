'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.del = exports.update = exports.getAllForFleet = exports.verify = exports.getByTelephoneNumber = exports.getAll = exports.addNewLocationWithAuthentication = exports.assignNumber = exports.post = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _location = require('../models/location');

var Location = _interopRequireWildcard(_location);

var _fleet = require('../models/fleet');

var Fleet = _interopRequireWildcard(_fleet);

var _boom = require('boom');

var _boom2 = _interopRequireDefault(_boom);

var _twoFactorCall = require('../services/twoFactorCall');

var _twoFactorCall2 = _interopRequireDefault(_twoFactorCall);

var _session = require('../services/session');

var _session2 = _interopRequireDefault(_session);

var _user = require('../models/user');

var User = _interopRequireWildcard(_user);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var post = exports.post = function post(request, reply) {
  var location = request.payload;
  var fleetId = request.params.fleetId;

  Fleet.get(fleetId).then(function (fleet) {
    if (!fleet) {
      return reply(_boom2.default.badRequest('fleet not found'));
    }

    location.fleetId = fleetId;

    Location.save(location).then(function (locationSaved) {
      (0, _twoFactorCall2.default)(fleet, locationSaved).then(function () {
        return console.log('calling ' + location.telephoneNumber);
      }).catch(function (e) {
        return console.log('error calling ' + location.telephoneNumber, e);
      });

      reply(locationSaved);
    }).catch(function (err) {
      console.log('Location save ERR', err);
      reply(_boom2.default.badImplementation());
    });
  }).catch(function (err) {
    if (err.notFound) {
      return reply(_boom2.default.notFound('Fleet not found id:' + fleetId));
    }

    console.log('fetching fleet ERR', err);
    reply(_boom2.default.badImplementation());
  });
};

var assignNumber = exports.assignNumber = function assignNumber(request, reply) {
  Location.addDirectNumber(request.params.id, request.payload.directNumber).then(function () {
    return reply();
  }).catch(function (e) {
    console.log(e.stack);
    reply(_boom2.default.badImplementation());
  });
};

var addNewLocationWithAuthentication = exports.addNewLocationWithAuthentication = function addNewLocationWithAuthentication(request, reply) {
  var location = request.payload;
  var fleetId = request.params.fleetId;

  Fleet.get(fleetId).then(function (fleet) {
    if (!fleet) {
      return reply(_boom2.default.badRequest('fleet not found'));
    }

    location.fleetId = fleetId;

    Location.save(_extends({}, location, { isVerified: true })).then(reply).catch(function (err) {
      console.log('Location save ERR', err);
      reply(_boom2.default.badImplementation());
    });
  }).catch(function (err) {
    if (err.notFound) {
      return reply(_boom2.default.notFound('Fleet not found id:' + fleetId));
    }

    console.log('fetching fleet ERR', err);
    reply(_boom2.default.badImplementation());
  });
};

var getAll = exports.getAll = function getAll(request, reply) {
  Location.all().then(function (locations) {
    var locationsToShow = locations.map(function (l) {
      return _lodash2.default.omit(l, ['fleetId', 'bookings', '_id']);
    });

    reply(locationsToShow);
  }).catch(function (err) {
    console.log('ERROR fetching all locations', err);
    reply(_boom2.default.notFound()); // not found for obscurity.
  });
};

var getByTelephoneNumber = exports.getByTelephoneNumber = function getByTelephoneNumber(request, reply) {
  Location.getByTelephoneNumber(request.params.number).then(function (location) {
    if (location) {
      reply(location);
    } else {
      reply(_boom2.default.notFound());
    }
  }).catch(function (err) {
    console.log(err);
    reply(_boom2.default.badImplementation());
  });
};

var verify = exports.verify = function verify(request, reply) {
  var id = request.params.id;
  var oneTimeCode = request.payload.oneTimeCode;


  _session2.default.get(id + '-' + oneTimeCode).then(function (result) {
    if (!result) {
      return reply(_boom2.default.badRequest('Invalid one time code'));
    }

    return Location.verify(id).then(function () {
      reply();
    });
  }).catch(function (e) {
    console.log(e);
    reply(_boom2.default.badImplementation(e));
  });
};

var getAllForFleet = exports.getAllForFleet = function getAllForFleet(request, reply) {
  var _id = request.auth.credentials.user._id;
  // we get user first as they may not have the fleet id in their token as it could be their first login.

  User.get(_id).then(function (_ref) {
    var fleetId = _ref.fleetId;

    if (!fleetId) {
      return reply([]);
    }

    Location.getAllForFleet(fleetId).then(reply);
  }).catch(function (e) {
    if (e.error === 'not_found') {
      return reply([]);
    }
    console.log('Location.getAllForFleet ERR', e);
    reply(_boom2.default.badImplementation());
  });
};

var update = exports.update = function update(request, reply) {
  var locationId = request.params.locationId;
  var payload = request.payload;
  var _id = request.auth.credentials.user._id;

  Promise.all([User.get(_id), Location.get(locationId)]).then(function (_ref2) {
    var _ref3 = _slicedToArray(_ref2, 2);

    var user = _ref3[0];
    var location = _ref3[1];

    if (user.fleetId !== location.fleetId) {
      return reply(_boom2.default.badRequest('You do not have permission to change this location', locationId));
    }

    Location.update(locationId, payload).then(function () {
      return reply();
    }).catch(function (e) {
      console.log('Location.update ERR', e);
      reply(_boom2.default.badImplementation());
    });
  }).catch(function (e) {
    console.log('Location.update, Promise.all, User.get, Location.get ERR', e);
    reply(_boom2.default.badImplementation());
  });
};

var del = exports.del = function del(request, reply) {
  var locationId = request.params.locationId;
  var _id = request.auth.credentials.user._id;

  Promise.all([User.get(_id), Location.get(locationId)]).then(function (_ref4) {
    var _ref5 = _slicedToArray(_ref4, 2);

    var user = _ref5[0];
    var location = _ref5[1];

    if (user.fleetId !== location.fleetId) {
      return reply(_boom2.default.badRequest('You do not have permission to change this location', locationId));
    }
    Location.del(locationId).then(function () {
      return reply();
    }).catch(function (e) {
      console.log('Location.del ERR', e);
      reply(_boom2.default.badImplementation());
    });
  }).catch(function (e) {
    console.log('Location.del, Promise.all, User.get, Location.get ERR', e);
    reply(_boom2.default.badImplementation());
  });
};