'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.activeUsers = exports.logout = exports.get = exports.forgot = exports.password = exports.login = exports.create = undefined;

var _user = require('../models/user');

var User = _interopRequireWildcard(_user);

var _boom = require('boom');

var _boom2 = _interopRequireDefault(_boom);

var _config = require('../config');

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _session = require('../services/session');

var _session2 = _interopRequireDefault(_session);

var _uuid = require('uuid');

var _uuid2 = _interopRequireDefault(_uuid);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var create = exports.create = function create(request, reply) {
  var _request$payload = request.payload;
  var fleetName = _request$payload.fleetName;
  var email = _request$payload.email;


  User.create(request.payload).then(function () {
    reply();
  }).catch(function (e) {
    if (e.message === 'EMAIL_EXISTS') {
      return reply(_boom2.default.conflict('Email Exists', email));
    }

    if (e.message === 'FLEET_NAME_EXISTS') {
      return reply(_boom2.default.conflict('Fleet Exists', fleetName));
    }
    console.log(e);
    reply(_boom2.default.badImplementation('Error creating new user', e));
  });
};

var login = exports.login = function login(request, reply) {
  var _request$payload2 = request.payload;
  var username = _request$payload2.username;
  var password = _request$payload2.password;


  User.login(username, password).then(function (user) {
    if (!user) {
      return reply(_boom2.default.unauthorized('Please check your username or password'));
    }
    delete user.hash;
    var sessionId = _uuid2.default.v4();
    var token = _jsonwebtoken2.default.sign({ accountId: user._id, user: user, sessionId: sessionId }, _config.JWT_SECRET_KEY, { expiresIn: _config.JWT_TOKEN_EXPIRY });

    _session2.default.set(sessionId, user).then(function () {
      reply(user).state(_config.AUTH_COOKIE_NAME, token);
    }).catch(function (e) {
      console.log(e);
      reply(_boom2.default.badImplementation());
    });
  }).catch(function (e) {
    console.log(e);
    reply(_boom2.default.badImplementation());
  });
};

var password = exports.password = function password(request, reply) {
  var code = request.params.code;

  console.log('code', code);
  _session2.default.get(code).then(function (user) {
    console.log('changing password', user);
    if (!user) {
      return reply(_boom2.default.badRequest('email timed out'));
    }
    User.updatePassword(user._id, request.payload.password).then(function () {
      return _session2.default.del(code);
    }).then(function () {
      return reply();
    });
  }).catch(function (e) {
    console.log('Error getting ${code} for password reset', e);
    reply(_boom2.default.badImplementation());
  });
};

var forgot = exports.forgot = function forgot(request, reply) {
  User.findByEmail(request.payload.username).then(function (res) {
    reply();
    if (res) {
      User.sendForgotEmail(res).catch(function (e) {
        return console.log('sendForgotEmail ERR', e);
      });
    }
  }).catch(function (e) {
    console.log('findByEmail ERR', e);
    reply(_boom2.default.badImplementation());
  });
};

var get = exports.get = function get(request, reply) {
  var _id = request.auth.credentials.user._id;


  User.getAccount(_id).then(reply).catch(function (e) {
    console.log('User.getAccount ERR', e);
    reply(_boom2.default.badImplementation());
  });
};

var logout = exports.logout = function logout(request, reply) {
  var cookie = request.state[_config.AUTH_COOKIE_NAME];
  var sessionId = request.auth.credentials.sessionId;

  _session2.default.del(sessionId);
  if (!cookie) {
    return reply();
  }

  reply({ message: 'You\'ve been successfully logged out' }).state(_config.AUTH_COOKIE_NAME, '', {
    ttl: -1,
    isHttpOnly: true
  });
};

var activeUsers = exports.activeUsers = function activeUsers(request, reply) {
  User.activeUsers().then(reply).catch(function (err) {
    console.log(err);
    reply(_boom2.default.badImplementation(err));
  });
};