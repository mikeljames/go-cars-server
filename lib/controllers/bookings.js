'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.cancel = exports.book = undefined;

var _booking = require('../models/booking');

var Booking = _interopRequireWildcard(_booking);

var _location = require('../models/location');

var Location = _interopRequireWildcard(_location);

var _fleet = require('../models/fleet');

var Fleet = _interopRequireWildcard(_fleet);

var _boom = require('boom');

var _boom2 = _interopRequireDefault(_boom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var book = exports.book = function book(_ref, reply) {
  var payload = _ref.payload;
  var params = _ref.params;

  // map timezone to fleet timezone
  // fleet timezone set by: moment.tz().names()
  var booking = payload;
  var locationId = params.locationId;

  Location.getByShortId(locationId).then(function (location) {
    if (!location) {
      return reply(_boom2.default.notFound());
    }

    Fleet.get(location.fleetId).then(function (fleet) {
      booking.address = location.address;
      booking.postcode = location.postcode;
      booking.coords = location.coords;
      booking.extraInstructions = location.extraInstructions;
      booking.telephoneNumber = booking.telephoneNumber || location.telephoneNumber;

      Booking.book({ booking: booking, fleet: fleet }).then(function (bookingReceipt) {
        reply(bookingReceipt);
      }).catch(function (err) {
        console.log('BOOKING ERR', err);
        reply(_boom2.default.badImplementation());
      });
    }).catch(function (err) {
      console.log('FLEET FETCH ERR', err);
      reply(_boom2.default.badImplementation());
    });
  }).catch(function (err) {
    if (err.notFound) {
      return reply(_boom2.default.notFound());
    }
    console.log('LOCATION getByShortId ERR', err);
    reply(_boom2.default.badImplementation());
  });
};

var cancel = exports.cancel = function cancel(request, reply) {
  var pk = request.params.pk;
  var _request$payload = request.payload;
  var telephoneNumber = _request$payload.telephoneNumber;
  var description = _request$payload.description;

  console.log(request.payload);
  Booking.cancel({ pk: pk, telephoneNumber: telephoneNumber, description: description }).then(reply).catch(function (e) {
    console.log('Booking.cancel ERR', e, 'pk', pk);
    reply(Booking.badImplementation());
  });
};