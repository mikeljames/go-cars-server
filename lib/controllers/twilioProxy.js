'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getNumbers = exports.summary = exports.releaseNumber = exports.purchaseNumber = exports.searchNumbers = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _config = require('../config');

var _fleet = require('../models/fleet');

var Fleet = _interopRequireWildcard(_fleet);

var _location = require('../models/location');

var Location = _interopRequireWildcard(_location);

var _user = require('../models/user');

var User = _interopRequireWildcard(_user);

var _boom = require('boom');

var _boom2 = _interopRequireDefault(_boom);

var _twilio = require('../services/twilio');

var twilio = _interopRequireWildcard(_twilio);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var searchNumbers = exports.searchNumbers = function searchNumbers(request, reply) {
  var _request$query = request.query;
  var number = _request$query.number;
  var isoCode = _request$query.isoCode;
  var accountId = request.auth.credentials.accountId;


  User.get(accountId).then(function (user) {
    return Fleet.get(user.fleetId);
  }).then(function (fleet) {
    if (!fleet.twilio.accountSid) {
      return reply(_boom2.default.badRequest('ACCOUNT_SID_NOT_SET'));
    }

    return twilio.searchNumber(fleet.twilio.accountSid, isoCode, number);
  }).then(reply).catch(function (e) {
    reply(_boom2.default.badImplementation());
    console.log(e.stack);
  });
};

var purchaseNumber = exports.purchaseNumber = function purchaseNumber(request, reply) {
  var accountId = request.auth.credentials.accountId;


  User.get(accountId).then(function (user) {
    return Fleet.get(user.fleetId);
  }).then(function (fleet) {
    if (!fleet.twilio.accountSid) {
      return reply(_boom2.default.badRequest('ACCOUNT_SID_NOT_SET'));
    }

    var friendlyName = fleet.fleetName + ' ' + request.payload.phoneNumber;

    return twilio.purchaseNumber({
      accountSid: fleet.twilio.accountSid,
      friendlyName: friendlyName,
      voiceUrl: process.env.NODE_ENV === 'development' ? 'https://voice.gocaller.co.uk/call' : _config.VOICE_API + '/tdispatch/call',
      phoneNumber: request.payload.phoneNumber
    }).then(function (twilioNumber) {
      var promise = Promise.resolve();

      if (request.payload.locationId) {
        promise = Location.addDirectNumber(request.payload.locationId, twilioNumber);
      }

      return promise.then(function () {
        return Fleet.addNewNumber(fleet._id, _extends({}, twilioNumber, {
          locationId: request.payload.locationId
        }));
      }).then(function () {
        return twilioNumber;
      });
    }).then(function (twilioNumber) {
      if (!fleet.twilio.number) {
        return Fleet.update(fleet._id, {
          twilio: _extends({}, fleet.twilio, {
            number: twilioNumber.phoneNumber
          })
        });
      }
    });
  }).then(function () {
    return reply();
  }).catch(function (e) {
    console.log(e);
    reply(_boom2.default.badImplementation());
    console.log(e.stack);
  });
};

var releaseNumber = exports.releaseNumber = function releaseNumber(request, reply) {
  var accountId = request.auth.credentials.accountId;


  User.get(accountId).then(function (user) {
    return Fleet.get(user.fleetId);
  }).then(function (fleet) {
    if (!fleet.twilio.accountSid) {
      return reply(_boom2.default.badRequest('ACCOUNT_SID_NOT_SET'));
    }

    return twilio.releaseNumber({
      accountSid: fleet.twilio.accountSid,
      numberSid: request.payload.sid
    }).then(function () {
      return Fleet.removeNumber(fleet._id, request.payload.sid);
    });
  }).then(function () {
    return reply();
  }).catch(function (e) {
    reply(_boom2.default.badImplementation());
    console.log(e.stack);
  });
};

var summary = exports.summary = function summary(request, reply) {
  var accountId = request.auth.credentials.accountId;


  User.get(accountId).then(function (user) {
    return Fleet.get(user.fleetId);
  }).then(function (fleet) {
    if (!fleet.twilio.accountSid) {
      return reply(_boom2.default.badRequest('ACCOUNT_SID_NOT_SET'));
    }

    return twilio.account(fleet.twilio.accountSid);
  }).then(reply).catch(function (e) {
    reply(_boom2.default.badImplementation());
    console.log(e.stack);
  });
};

var getNumbers = exports.getNumbers = function getNumbers(request, reply) {
  var accountId = request.auth.credentials.accountId;


  User.get(accountId).then(function (user) {
    return Fleet.get(user.fleetId);
  }).then(function (fleet) {
    if (!fleet.twilio.accountSid) {
      return reply(_boom2.default.badRequest('ACCOUNT_SID_NOT_SET'));
    }
    return twilio.numbers(fleet.twilio.accountSid);
  })
  // return just the array and not all the other crap
  .then(function (result) {
    return result.incomingPhoneNumbers;
  }).then(reply).catch(function (e) {
    reply(_boom2.default.badImplementation());
    console.log(e.stack);
  });
};