'use strict';

require('newrelic');

var _hapi = require('hapi');

var _hapi2 = _interopRequireDefault(_hapi);

var _config = require('./config');

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

var _validateToken = require('./validateToken');

var _validateBasic = require('./validateBasic');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isTestMode = process.env.TEST_MODE === 'true';
console.log('isTestMode', isTestMode);

var server = new _hapi2.default.Server();

server.connection({
  port: _config.PORT,
  routes: { cors: { credentials: true } }
});

server.state(_config.AUTH_COOKIE_NAME, {
  ttl: _config.AUTH_COOKIE_TTL,
  isSecure: _config.NODE_ENV === 'production',
  isHttpOnly: true,
  clearInvalid: false, // remove invalid cookies
  strictHeader: true, // don't allow violations of RFC 6265
  path: '/'
});

server.register([require('bell'), require('hapi-auth-jwt2'), require('hapi-auth-basic')], function (err) {

  if (err) {
    console.log(err);
    return process.exit(1);
  }

  // Declare an authentication strategy using the bell scheme
  // with the name of the provider, cookie encryption password,
  // and the OAuth client credentials.
  // server.auth.strategy('facebook', 'bell', {
  //   provider: 'facebook',
  //   password: 'fa23sadHKUCOPVdSDhdsJdkS!23',
  //   clientId: '1674324019478742',
  //   clientSecret: 'f0bb35b70f1b15be90265798fabf94c6',
  //   isSecure: NODE_ENV === 'production'
  // });
  server.auth.strategy('simple', 'basic', { validateFunc: _validateBasic.validateBasic });
  server.auth.strategy('jwt', 'jwt', { key: _config.JWT_SECRET_KEY, // Never Share your secret key
    validateFunc: _validateToken.validateToken,
    cookieKey: 'jwt', // validate function defined above
    verifyOptions: { algorithms: ['HS256'] } // pick a strong algorithm
  });

  server.auth.default('jwt');

  server.route(_routes2.default);

  server.start(function (e) {
    if (e) {
      console.log(e);
      return process.exit(1);
    }

    console.log('Server Listening on PORT', _config.PORT);
  });
});

var exit = function exit() {
  process.exit(0);
};

process.on('SIGINT', exit);
process.on('SIGTERM', exit);