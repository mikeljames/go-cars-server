'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.update = exports.get = exports.del = exports.getAllForFleet = exports.all = exports.verify = exports.addDirectNumber = exports.getByTelephoneNumber = exports.getByShortId = exports.save = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _randomString = require('random-string');

var _randomString2 = _interopRequireDefault(_randomString);

var _nodeUuid = require('node-uuid');

var _nodeUuid2 = _interopRequireDefault(_nodeUuid);

var _booking = require('./booking');

var _couch = require('../helpers/couch');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TYPE = 'Location';

var save = exports.save = function save(_ref) {
  var address = _ref.address;
  var postcode = _ref.postcode;
  var coords = _ref.coords;
  var extraInstructions = _ref.extraInstructions;
  var fleetId = _ref.fleetId;
  var bookings = _ref.bookings;
  var shortId = _ref.shortId;
  var isVerified = _ref.isVerified;
  var name = _ref.name;
  var telephoneNumber = _ref.telephoneNumber;

  return (0, _couch.couchSave)(_nodeUuid2.default.v4(), {
    type: TYPE,
    address: address,
    postcode: postcode,
    coords: coords,
    extraInstructions: extraInstructions,
    fleetId: fleetId,
    bookings: bookings || [],
    shortId: shortId || (0, _randomString2.default)({ length: 6, numeric: true }),
    isVerified: isVerified || false,
    name: name,
    telephoneNumber: telephoneNumber,
    deleted: false
  }).then(function (result) {
    return get(result.id);
  }).then(function (location) {
    return _extends({}, location, {
      id: location._id
    });
  });
};

var getByShortId = exports.getByShortId = function getByShortId(key) {
  return (0, _couch.couchView)(TYPE + '/getByShortId', { key: key }, { value: true }).then(function (_ref2) {
    var _ref3 = _slicedToArray(_ref2, 1);

    var first = _ref3[0];
    return first;
  });
};

var getByTelephoneNumber = exports.getByTelephoneNumber = function getByTelephoneNumber(locationNumber, oneToOneCallSessionNumber) {
  // ensure it works for temporary users so they can hear a list of their own bookings
  if (oneToOneCallSessionNumber) {
    return Promise.all([(0, _couch.couchView)(TYPE + '/getByTelephoneNumber', { key: locationNumber }, { value: true }), (0, _booking.getCurrentBookings)(oneToOneCallSessionNumber)]).then(function (_ref4) {
      var _ref5 = _slicedToArray(_ref4, 2);

      var _ref5$ = _slicedToArray(_ref5[0], 1);

      var location = _ref5$[0];
      var bookings = _ref5[1];

      return _extends({}, location, {
        bookings: bookings
      });
    }).catch(function () {
      // return nothing as not found
      return {};
    });
  }
  return Promise.all([(0, _couch.couchView)(TYPE + '/getByTelephoneNumber', { key: locationNumber }, { value: true }), (0, _booking.getCurrentBookings)(locationNumber)]).then(function (_ref6) {
    var _ref7 = _slicedToArray(_ref6, 2);

    var _ref7$ = _slicedToArray(_ref7[0], 1);

    var location = _ref7$[0];
    var bookings = _ref7[1];

    return _extends({}, location, {
      bookings: bookings
    });
  }).catch(function () {
    return {};
  });
};

var addDirectNumber = exports.addDirectNumber = function addDirectNumber(id, number) {
  return (0, _couch.couchUpdate)(id, {
    directNumber: number.phoneNumber,
    sid: number.sid
  });
};

var verify = exports.verify = function verify(id) {
  return (0, _couch.couchUpdate)(id, { isVerified: true });
};

// super-admin view
var all = exports.all = function all() {
  return (0, _couch.couchView)(TYPE + '/all', null, { value: true });
};

var getAllForFleet = exports.getAllForFleet = function getAllForFleet(key) {
  return (0, _couch.couchView)(TYPE + '/getAllForFleet', { key: key }, { value: true });
};

var del = exports.del = function del(id) {
  return (0, _couch.couchUpdate)(id, { deleted: true });
};

var get = exports.get = function get(key) {
  return (0, _couch.couchTypeGet)(key, TYPE);
};
var update = exports.update = _couch.couchUpdate;

(0, _couch.couchDesignDoc)(TYPE, {
  all: {
    map: 'function(doc) {\n      if (doc.type === "' + TYPE + '") {\n        emit(doc.id, doc);\n      }\n    }'
  },
  getNonVerified: {
    map: 'function(doc) {\n      if (!doc.isVerified && doc.type === "' + TYPE + '") {\n        emit(doc.id, doc);\n      }\n    }'
  },
  getByShortId: {
    map: 'function(doc) {\n      if (doc.type === "' + TYPE + '" && doc.isVerified && doc.deleted === false) {\n        emit(doc.shortId, doc);\n      }\n    }'
  },
  getAllForFleet: {
    map: 'function(doc) {\n      if (doc.type === "' + TYPE + '" && doc.deleted === false && doc.isVerified) {\n        emit(doc.fleetId, doc);\n      }\n    }'
  },
  getByTelephoneNumber: {
    map: 'function(doc) {\n      if (doc.type === "' + TYPE + '" && doc.isVerified && doc.deleted === false) {\n        if (doc.directNumber) {\n          emit(doc.directNumber, doc);\n        }\n        emit(doc.telephoneNumber, doc);\n      }\n    }'
  }
});