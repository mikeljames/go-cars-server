'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.get = exports.activeUsers = exports.sanitizeUser = exports.getAccount = exports.updatePassword = exports.update = exports.userHasActivatedFleet = exports.login = exports.findByEmail = exports.sendForgotEmail = exports.create = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _couch = require('../helpers/couch');

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _bluebird = require('bluebird');

var _session = require('../services/session');

var _session2 = _interopRequireDefault(_session);

var _nodeUuid = require('node-uuid');

var _nodeUuid2 = _interopRequireDefault(_nodeUuid);

var _fleet = require('./fleet');

var Fleet = _interopRequireWildcard(_fleet);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _Email = require('../services/Email');

var _Email2 = _interopRequireDefault(_Email);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isTestMode = process.env.TEST_MODE === 'true';

var comparePassword = (0, _bluebird.promisify)(_bcrypt2.default.compare);

var hashFn = (0, _bluebird.promisify)(_bcrypt2.default.hash);
var TYPE = 'User';

var create = exports.create = function create(_ref) {
  var name = _ref.name;
  var fleetName = _ref.fleetName;
  var email = _ref.email;
  var password = _ref.password;


  return Promise.all([(0, _couch.couchView)(TYPE + '/email', {
    key: email
  }), (0, _couch.couchView)('Fleet/name', {
    key: fleetName
  })]).then(function (_ref2) {
    var _ref3 = _slicedToArray(_ref2, 2);

    var user = _ref3[0];
    var fleet = _ref3[1];


    if (user.length) {
      throw Error('EMAIL_EXISTS');
    }

    if (fleet.length) {
      throw Error('FLEET_NAME_EXISTS');
    }

    return hashFn(password, 10).then(function (hash) {
      var id = _nodeUuid2.default.v4();
      sendWelcomeEmail({ name: name, fleetName: fleetName, email: email });
      return (0, _couch.couchSave)(id, {
        name: name, fleetName: fleetName, email: email, hash: hash, role: 'fleet-admin', type: 'User'
      });
    });
  });
};

var sendWelcomeEmail = function sendWelcomeEmail(_ref4) {
  var name = _ref4.name;
  var fleetName = _ref4.fleetName;
  var email = _ref4.email;
  return (0, _Email2.default)({
    email: email,
    html: (0, _Email.welcomeTemplate)({
      name: name,
      fleetName: fleetName,
      link: 'https://app.gocaller.co.uk'
    }),
    subject: '🚕  Welcome to Go Caller'
  });
};

var sendForgotEmail = exports.sendForgotEmail = function sendForgotEmail(user) {
  var id = !isTestMode ? _nodeUuid2.default.v4() : '1a5ae654-6388-47f4-b7b4-641f01bfb248';
  console.log('ID for email', id);
  (0, _Email2.default)({
    to: user.email,
    html: (0, _Email.forgotTemplate)({ link: 'https://app.gocaller.co.uk/passwordreset/' + id }),
    subject: '🚕  Forgot Password - Go Caller'
  });

  return _session2.default.set(id, user);
  // .then(send email)
};

var findByEmail = exports.findByEmail = function findByEmail(key) {
  return (0, _couch.couchView)(TYPE + '/email', { key: key }, { value: true }).then(function (_ref5) {
    var _ref6 = _slicedToArray(_ref5, 1);

    var first = _ref6[0];
    return first;
  });
};

var login = exports.login = function login(username, password) {
  return (0, _couch.couchView)(TYPE + '/email', {
    key: username
  }).then(function (_ref7) {
    var _ref8 = _slicedToArray(_ref7, 1);

    var res = _ref8[0];

    if (!res) {
      return null;
    }

    var user = res.value;
    return comparePassword(password, user.hash).then(function (comparison) {
      if (comparison) {
        return user;
      }
      return null;
    });
  });
};

var userHasActivatedFleet = exports.userHasActivatedFleet = function userHasActivatedFleet(userId, fleet) {
  return (0, _couch.couchUpdate)(userId, { fleetId: fleet.id, fleetActivated: true });
};

var update = exports.update = function update(id, newProps) {
  return (0, _couch.couchUpdate)(id, newProps);
};

var updatePassword = exports.updatePassword = function updatePassword(id, password) {
  return hashFn(password, 10).then(function (hash) {
    return update(id, { hash: hash });
  });
};

var getAccount = exports.getAccount = function getAccount(id) {
  return get(id).then(sanitizeUser).then(function (user) {
    if (!user.fleetId) {
      return Object.assign({}, user, {
        fleet: null,
        fleetId: null
      });
    }

    return Fleet.get(user.fleetId).then(Fleet.sanitizeFleet).then(function (fleet) {
      return Object.assign({}, user, {
        fleet: fleet
      });
    });
  });
};

var sanitizeUser = exports.sanitizeUser = function sanitizeUser(user) {
  return _lodash2.default.omit(user, ['hash']);
};

var activeUsers = exports.activeUsers = function activeUsers() {
  return (0, _couch.couchView)(TYPE + '/activeUsers');
};

var get = exports.get = function get(id) {
  return (0, _couch.couchTypeGet)(id, TYPE);
};

(0, _couch.couchDesignDoc)(TYPE, {
  email: {
    map: 'function (doc) {\n\t\t\tif(doc.type === "' + TYPE + '") {\n\t\t\t\temit(doc.email, doc);\n\t\t\t}\n\t\t}'
  },
  activeUsers: {
    map: 'function (doc) {\n      if(doc.type === "' + TYPE + '" && doc.fleetActivated) {\n        emit(doc.id, doc);\n      }\n    }'
  }
});