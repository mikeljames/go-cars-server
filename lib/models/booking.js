'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getBookingFromWebhookPayload = exports.cancel = exports.getCurrentBookings = exports.book = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _couch = require('../helpers/couch');

var _config = require('../config');

var Config = _interopRequireWildcard(_config);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _tdispatchOauth = require('../services/tdispatch-oauth');

var _tdispatchOauth2 = _interopRequireDefault(_tdispatchOauth);

var _camelcaseKeysRecursive = require('camelcase-keys-recursive');

var _camelcaseKeysRecursive2 = _interopRequireDefault(_camelcaseKeysRecursive);

var _session = require('../services/session');

var _session2 = _interopRequireDefault(_session);

var _fleet = require('./fleet');

var Fleet = _interopRequireWildcard(_fleet);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var TYPE = 'Booking';

// update to allow Oauth customers to book.
var book = exports.book = function book(_ref) {
  var booking = _ref.booking;
  var fleet = _ref.fleet;

  return auth(fleet).then(_book(booking)).then(function (bookingResult) {
    var telephoneNumber = booking.telephoneNumber;

    return storeToCache({
      telephoneNumber: telephoneNumber,
      booking: bookingResult,
      fleetId: fleet.id,
      locationId: booking.locationId
    }).then(function () {
      return bookingResult;
    });
  });
};

var auth = function auth(fleet) {
  return new _tdispatchOauth2.default({
    username: fleet.defaultCustomer.email,
    password: fleet.defaultCustomer.password,
    secret: fleet.secret,
    key: fleet.key,
    clientId: fleet.clientId
  }).getAccessToken();
};

var _book = function _book(booking) {
  return function (_ref2) {
    var accessToken = _ref2.accessToken;


    var uri = _url2.default.resolve(Config.Tdispatch.url, '/passenger/v1/bookings?access_token=' + accessToken);
    return _requestPromise2.default.post({
      uri: uri,
      timeout: 20000, // Tdispatch can be slow.
      body: {
        passenger: {
          name: booking.passengerName,
          phone: booking.telephoneNumber
        },
        pickup_time: booking.pickUpTime,
        pickup_location: {
          postcode: booking.postcode,
          location: {
            lat: booking.coords.lat,
            lng: booking.coords.lng
          },
          address: booking.address
        },
        passengers: booking.passengerCount,
        extra_instructions: booking.extra_instructions,
        payment_method: 'cash',
        status: 'incoming',
        autoDispatch: true
      },
      json: true
    }).then(_camelcaseKeysRecursive2.default).then(function (bookingResponse) {
      return bookingResponse.booking;
    }); // tdispatch wraps its responses in 200 ok status codes etc. eurgh
  };
};

var storeToCache = function storeToCache(_ref3) {
  var telephoneNumber = _ref3.telephoneNumber;
  var booking = _ref3.booking;
  var fleetId = _ref3.fleetId;

  var tel = telephoneNumber.replace('+', '');
  console.log('telephoneNumber', tel);
  return _session2.default.get(tel).then(function (bookingsMade) {
    return !Array.isArray(bookingsMade) ? [] : bookingsMade;
  }).then(function (bookingsMade) {
    console.log('booking.key', booking.key);
    console.log('bookingsMade', bookingsMade);
    // ensure booking doesn't exist already for any reason
    var existingBooking = bookingsMade.filter(function (b) {
      return b.key === booking.key;
    });
    if (existingBooking.length) {
      console.log('booking exists in cache', booking.key);
      // booking exists in cache already so just resolve and forget
      return Promise.resolve();
    }
    var bookingsMadeUpdated = [].concat(_toConsumableArray(bookingsMade), [Object.assign({}, booking, {
      fleetId: fleetId
    })]);

    return _session2.default.set(tel, bookingsMadeUpdated, _config.CALL_CACHE_TTL);
  });
};

var getCurrentBookings = exports.getCurrentBookings = function getCurrentBookings(tel) {
  return _session2.default.get(tel.replace('+', '')).then(function (bookings) {
    if (!bookings) {
      return [];
    }
    return bookings;
  });
};

var cancel = exports.cancel = function cancel(_ref4) {
  var pk = _ref4.pk;
  var telephoneNumber = _ref4.telephoneNumber;
  var description = _ref4.description;

  var tel = telephoneNumber.replace('+', '');
  return _session2.default.get(tel).then(function (bookings) {
    if (!bookings) throw new Error('No Bookings for ' + telephoneNumber);
    console.log('bookings', bookings);
    console.log('typeof bookings', typeof bookings === 'undefined' ? 'undefined' : _typeof(bookings));
    var booking = bookings.find(function (b) {
      return b.pk === pk;
    });
    if (!booking) throw new Error('No Bookings for ' + pk);

    return Fleet.get(booking.fleetId).then(auth).then(_camelcaseKeysRecursive2.default).then(_cancel(pk, description)).then(function () {
      if (bookings.length === 1) {
        return _session2.default.del(tel);
      }
      var bookingsRecord = bookings.find(function (b) {
        return b.pk !== pk;
      });
      return _session2.default.set(tel, bookingsRecord);
    });
  });
};

var _cancel = function _cancel(pk, description) {
  return function (_ref5) {
    var accessToken = _ref5.accessToken;

    var url = _url2.default.resolve(Config.Tdispatch.url, '/passenger/v1/bookings/' + pk + '/cancel?access_token=' + accessToken);
    return _requestPromise2.default.post({
      url: url,
      headers: {
        'Content-Type': 'application/json'
      },
      timeout: 20000, // Tdispatch can be slow.
      body: {
        description: description
      },
      json: true
    }).then(_camelcaseKeysRecursive2.default);
  };
};

var getBookingFromWebhookPayload = exports.getBookingFromWebhookPayload = function getBookingFromWebhookPayload(fleet) {
  return function (webhookPayload) {
    return auth(fleet).then(function (_ref6) {
      var accessToken = _ref6.accessToken;

      return _getBooking(webhookPayload.bookingPk, accessToken).then(_camelcaseKeysRecursive2.default);
    });
  };
};

var _getBooking = function _getBooking(pk, accessToken) {
  var uri = _url2.default.resolve(Config.Tdispatch.url, '/passenger/v1/bookings/' + pk + '?access_token=' + accessToken);
  return _requestPromise2.default.get({
    uri: uri,
    json: true
  });
};

// resolveUrl(apiUrl, `/passenger/v1/bookings/${bookingHookPayload.booking_pk}?access_token=${accessToken.access_token}`
// const get = (pk)

(0, _couch.couchDesignDoc)(TYPE, {
  all: {
    map: 'function(doc) {\n      if (doc.type === "' + TYPE + '") {\n        emit(doc.id, doc);\n      }\n    }'
  }
});