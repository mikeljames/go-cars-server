'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.saveCustomer = undefined;

var _tdispatchCrm = require('../services/tdispatch-crm');

var _couch = require('../helpers/couch');

var _nodeUuid = require('node-uuid');

var _nodeUuid2 = _interopRequireDefault(_nodeUuid);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TYPE = 'Customer';

var saveCustomer = exports.saveCustomer = function saveCustomer(opts, customer) {
  var key = opts.key;
  var clientId = opts.clientId;
  var firstName = customer.firstName;
  var lastName = customer.lastName;
  var password = customer.password;
  var email = customer.email;


  var payload = {
    email: email,
    firstName: firstName,
    lastName: lastName,
    password: password,
    key: key,
    clientId: clientId
  };

  var id = _nodeUuid2.default.v4();
  return (0, _tdispatchCrm.create)({ key: key, clientId: clientId }, payload).then(function (_ref) {
    var passenger = _ref.passenger;

    return (0, _couch.couchSave)(id, {
      email: email,
      firstName: firstName,
      lastName: lastName,
      password: password,
      key: key,
      clientId: clientId,
      type: TYPE
    }).then(function () {
      return passenger;
    });
  });
};

(0, _couch.couchDesignDoc)(TYPE, {
  all: {
    map: 'function(doc) {\n      if (doc.type === "' + TYPE + '") {\n        emit(doc.id, doc);\n      }\n    }'
  }
});