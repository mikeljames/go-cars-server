'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.removeNumber = exports.getFleetNumbers = exports.addNewNumber = exports.sanitizeFleet = exports.getByCalled = exports.get = exports.update = exports.getAll = exports.getFleetByKeyHash = exports.save = exports.findByName = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _couch = require('../helpers/couch');

var _customer = require('./customer');

var _config = require('../config');

var _sha = require('../helpers/sha1');

var _sha2 = _interopRequireDefault(_sha);

var _randomString = require('random-string');

var _randomString2 = _interopRequireDefault(_randomString);

var _nodeUuid = require('node-uuid');

var _nodeUuid2 = _interopRequireDefault(_nodeUuid);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TYPE = 'Fleet';

var findByName = exports.findByName = function findByName(key) {
  return (0, _couch.couchView)(TYPE + '/name', { key: key });
};

var save = exports.save = function save(fleet) {
  return saveFleetCustomer(fleet).then(saveFleet(fleet));
};

// const auth = (fleet) => {
//   return new TdispatchAuth({
//     username: fleet.defaultCustomer.email,
//     password: fleet.defaultCustomer.password,
//     secret: fleet.secret,
//     key: fleet.key,
//     clientId: fleet.clientId
//   }).getAccessToken();
// };https://github.com/TDispatch/Passenger-API/wiki/v1-ref-Office-Time

var random = function random() {
  return Math.floor(Math.random() * 100) + Math.floor(Math.random() * 100);
};
var saveFleetCustomer = function saveFleetCustomer(_ref) {
  var fleetName = _ref.fleetName;
  var key = _ref.key;
  var clientId = _ref.clientId;

  var customerPayload = {
    email: _config.defaultCustomer.email.replace('{fleetName}', fleetName.replace(/\s/g, '') + '-' + random() + '-' + random()),
    firstName: fleetName,
    lastName: _config.defaultCustomer.lastName,
    password: _nodeUuid2.default.v4()
  };

  return (0, _customer.saveCustomer)({
    key: key,
    clientId: clientId
  }, customerPayload).then(function (doc) {
    return _extends({}, doc, customerPayload);
  });
};

var saveFleet = function saveFleet(fleet) {
  return function (customer) {
    var keyHash = (0, _sha2.default)(fleet.key);
    var payload = _extends({}, fleet, {
      twilio: {
        accountSid: fleet.twilio.accountSid,
        voice: 'alice'
      },
      refreshToken: customer.refreshToken,
      accessToken: customer.accessToken,
      keyHash: keyHash,
      defaultCustomer: customer,
      twilioConnect: !!fleet.twilio.accountSid,
      webhookKey: (0, _randomString2.default)({ length: 16 }),
      type: TYPE
    });

    return (0, _couch.couchSave)(_nodeUuid2.default.v4(), payload);
  };
};

var getFleetByKeyHash = exports.getFleetByKeyHash = function getFleetByKeyHash(key) {
  return (0, _couch.couchView)(TYPE + '/getByKeyHash', { key: key }, { value: true }).then(function (_ref2) {
    var _ref3 = _slicedToArray(_ref2, 1);

    var fleet = _ref3[0];
    return fleet;
  });
};

var getAll = exports.getAll = function getAll() {
  return (0, _couch.couchView)(TYPE + '/all');
};

var update = exports.update = function update(id, payload) {
  if (payload.key) {
    payload.keyHash = (0, _sha2.default)(payload.key);
  }

  console.log('update payload', payload);
  return (0, _couch.couchUpdate)(id, payload);
};

var get = exports.get = function get(key) {
  return (0, _couch.couchTypeGet)(key, TYPE);
};

var getByCalled = exports.getByCalled = function getByCalled(key) {
  return (0, _couch.couchView)(TYPE + '/getByCalled', { key: key }, { value: true }).then(function (_ref4) {
    var _ref5 = _slicedToArray(_ref4, 1);

    var fleet = _ref5[0];
    return fleet;
  });
};

var sanitizeFleet = exports.sanitizeFleet = function sanitizeFleet(fleet) {
  return Object.assign({}, fleet, {
    defaultCustomer: _lodash2.default.omit(fleet.defaultCustomer, ['password'])
  });
};

var addNewNumber = exports.addNewNumber = function addNewNumber(fleetId, number) {
  return (0, _couch.couchSave)(number.phoneNumber, _extends({}, number, {
    fleetId: fleetId,
    type: 'FLEET_NUMBER'
  }));
};

var getFleetNumbers = exports.getFleetNumbers = function getFleetNumbers(fleetId) {
  return (0, _couch.couchView)('FLEET_NUMBER/numbersForFleet', { key: fleetId }, { value: true });
};

var removeNumber = exports.removeNumber = function removeNumber(fleetId, numberSid) {
  return (0, _couch.couchView)('FLEET_NUMBER/numberBySid', { key: numberSid }, { value: true }).then(function (numbers) {
    /* eslint-disable consistent-return */
    if (!numbers || !numbers.length) {
      return;
    }
    var number = numbers[0];
    var del = (0, _couch.couchDelete)(number.phoneNumber);

    if (number.locationId) {
      del.then(function () {
        return (0, _couch.couchUpdate)(number.locationId, { directNumber: null });
      });
    }

    return del;
  });
};

(0, _couch.couchDesignDoc)(TYPE, {
  all: {
    map: 'function(doc) {\n      if (doc.type === "' + TYPE + '") {\n        emit(doc.id, doc);\n      }\n    }'
  },
  getByKeyHash: {
    map: 'function(doc) {\n      if (doc.type === "' + TYPE + '") {\n        emit(doc.keyHash, doc);\n      }\n    }'
  },
  getByCalled: {
    map: 'function(doc) {\n      if (doc.type === "' + TYPE + '") {\n        if(doc.twilio && doc.twilio.number) {\n          emit(doc.twilio.number, doc);\n        }\n      }\n    }'
  },
  name: {
    map: 'function(doc) {\n      if (doc.type === "' + TYPE + '") {\n        emit(doc.fleetName, doc);\n      }\n    }'
  }
});

(0, _couch.couchDesignDoc)('FLEET_NUMBER', {
  numberBySid: {
    map: 'function(doc) {\n      if (doc.type === "FLEET_NUMBER") {\n        emit(doc.sid, doc);\n      }\n    }'
  },
  numbersForFleet: {
    map: 'function(doc) {\n      if (doc.type === "FLEET_NUMBER") {\n        emit(doc.fleetId, doc)\n      }\n    }'
  }
});