'use strict';

// a functional approach to building out this api
// by no means complete, just an example on what a functional structure could yeild
var _ = require('lodash');
var curry = _.curry;
var rp = require('request-promise');

function newBooking(request, reply) {
  book(request.payload).then(function (booking) {

    reply();
  });
}

function book(booking) {

  getLocationByShortId(booking.locationId).then(function (location) {

    // no side effects
    var bookingPayload = Object.assign({}, booking, {
      location: location
    });

    // generic replacement
    return tdispatchBook(bookingPayload);
  });
}

function getLocationByShortId(shortId) {

  return new Promise(function (resolve, reject) {

    Db.view('Location/getByShortId', {
      key: shortId
    }, function (err, res) {
      if (err) {
        return reject(err);
      }
      var resCloned = void 0;

      if (res && _.isArray(res) && !res.length) {
        reject({
          notFound: true
        });
      } else if (res && res.length) {

        resCloned = _.clone(_.first(res).value);
        resCloned.id = res.id;
        resolve(resCloned);
      } else {

        reject('unrecognised response');
      }
    });
  });
}

function tdispatchBook(booking) {
  var credentials = booking.location.fleet.credentials;


  auth(credentials).then(curry(book)(booking));
}

function bookWithTDispatch(booking, token) {
  return rp({
    method: 'POST',
    url: API_URL + '/book',
    body: JSON.stringify({
      passengerName: booking.passengerName,
      telephoneNumber: booking.telephoneNumber,
      postcode: booking.postcode,
      address: booking.address,
      pickUpTime: booking.pickUpTime
    })
  }).then(okResponse).then(parseBody);
}

function auth(credentials) {
  return stepOne(credentials).then(getTokens);
}

function stepOne(credentials) {
  return rp({
    url: API_URL + '/auth',
    body: JSON.stringify(credentials),
    method: 'POST'
  }).then(okResponse).then(parseBody);
}

function getTokens(interMediateResponse) {
  return rp({
    url: API_URL + '/gettokens',
    method: 'GET'
  }).then(okResponse).then(parseBody);
}