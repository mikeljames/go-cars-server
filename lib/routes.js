'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _bookings = require('./controllers/bookings');

var BookingsController = _interopRequireWildcard(_bookings);

var _fleets = require('./controllers/fleets');

var FleetsController = _interopRequireWildcard(_fleets);

var _locations = require('./controllers/locations');

var LocationsController = _interopRequireWildcard(_locations);

var _tdispatchWebhook = require('./controllers/tdispatch-webhook');

var _tdispatchWebhook2 = _interopRequireDefault(_tdispatchWebhook);

var _users = require('./controllers/users');

var Users = _interopRequireWildcard(_users);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _protect = require('./helpers/protect');

var _protect2 = _interopRequireDefault(_protect);

var _Email = require('./services/Email');

var _twilioProxy = require('./controllers/twilioProxy');

var TwilioProxy = _interopRequireWildcard(_twilioProxy);

var _identifyCaller = require('./controllers/identifyCaller');

var _identifyCaller2 = _interopRequireDefault(_identifyCaller);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

exports.default = [{
  method: 'POST',
  path: '/fleet/user',
  handler: Users.create,
  config: {
    validate: {
      payload: {
        name: _joi2.default.string().min(1).required(),
        fleetName: _joi2.default.string().required(),
        email: _joi2.default.string().email().required(),
        password: _joi2.default.string().regex(/^[a-zA-Z0-9]{8,30}$/)
      }
    },
    auth: false
  }
}, {
  method: 'GET',
  path: '/fleet/user',
  handler: Users.get
}, {
  method: 'GET',
  path: '/fleet/{id}',
  handler: FleetsController.getById,
  config: {
    validate: {
      params: {
        id: _joi2.default.string().guid()
      }
    },
    auth: 'simple'
  }
}, {
  method: 'GET',
  path: '/fleets/{id}',
  handler: FleetsController.getFleetForPublicConsumption,
  config: {
    validate: {
      params: {
        id: _joi2.default.string().guid()
      }
    },
    auth: false
  }
}, {
  method: 'POST',
  path: '/fleet/user/login',
  handler: Users.login,
  config: {
    validate: {
      payload: {
        username: _joi2.default.string().email(),
        password: _joi2.default.string().regex(/^[a-zA-Z0-9]{8,30}$/)
      }
    },
    auth: false
  }
}, {
  method: 'POST',
  path: '/fleet/user/logout',
  handler: Users.logout,
  config: {
    auth: 'jwt'
  }
}, {
  method: 'PUT',
  path: '/fleet/user/password/{code}',
  handler: Users.password,
  config: {
    validate: {
      payload: {
        password: _joi2.default.string().regex(/^[a-zA-Z0-9]{8,30}$/)
      },
      params: {
        code: _joi2.default.string().guid()
      }
    },
    auth: false
  }
}, {
  method: 'POST',
  path: '/fleet/user/forgot',
  handler: Users.forgot,
  config: {
    validate: {
      payload: {
        username: _joi2.default.string().email()
      }
    },
    auth: false
  }
},
// alternative we copy over the twilio proxy *
// we preserve the current routes and structure *
// by the user being shown twilio before anything else in app
// we check for authToken in the model within the two factor calls *
// client app shows twilio connect if new twilioConnect flag is falsy
// client requests to post fleet setting twilio and t-dispatch setting twilioConnect flag to true *
// if fleet.twilio.authToken exists we put to the api setting twilioConnect flag to true *
// client continues normally
// voice remains unchanged *
// calls to twilio will need to check for *
// if !fleet.twilio.twilioConnect then simply use old call method *
// if fleet.twilio.twilioConnect then use new call method *
// wait for users to pass by and update then remove the flag.
// add unit tests to controllers
{
  method: 'POST',
  path: '/fleet',
  handler: FleetsController.post,
  config: {
    validate: {
      payload: {
        key: _joi2.default.string().min(32).max(32).required(),
        clientId: _joi2.default.string().min(8).required(),
        secret: _joi2.default.string().min(32).max(32).required(),
        fleetName: _joi2.default.string().required(),
        twilio: _joi2.default.object().keys({
          accountSid: _joi2.default.string().required()
        }),
        appEnabled: _joi2.default.bool().default(false),
        website: _joi2.default.string().default(null),
        language: _joi2.default.string().regex(/en|da-DK|de-DE|en-AU|en-CA|en-GB|en-IN|en-US|ca-ES|es-ES|es-MX|fi-FI|fr-CA|fr-FR|it-IT|ja-JP|ko-KR|nb-NO|nl-NL|pl-PL|pt-BR|pt-PT|ru-RU|sv-SE|zh-CN|zh-HK|zh-TW/g, 'valid language code eg. en-GB').default('en')
      }
    }
  }
}, {
  method: 'PUT',
  path: '/fleet/{id}',
  handler: FleetsController.put,
  config: {
    validate: {
      payload: {
        key: _joi2.default.string().min(32).max(32),
        clientId: _joi2.default.string().min(8),
        secret: _joi2.default.string().min(32).max(32),
        twilio: _joi2.default.object().keys({
          accountSid: _joi2.default.string()
        }),
        voice: _joi2.default.object().keys({
          greeting: _joi2.default.string(),
          callback: _joi2.default.string(),
          notRegistered: _joi2.default.string()
        }),
        appEnabled: _joi2.default.bool().default(false),
        website: _joi2.default.string().default(null),
        language: _joi2.default.string().regex(/en|da-DK|de-DE|en-AU|en-CA|en-GB|en-IN|en-US|ca-ES|es-ES|es-MX|fi-FI|fr-CA|fr-FR|it-IT|ja-JP|ko-KR|nb-NO|nl-NL|pl-PL|pt-BR|pt-PT|ru-RU|sv-SE|zh-CN|zh-HK|zh-TW/g, 'valid language code eg. en-GB').default('en')
      },
      params: {
        id: _joi2.default.string().guid()
      }
    }
  }
}, {
  method: 'PATCH',
  path: '/fleet/{id}',
  handler: FleetsController.put,
  config: {
    validate: {
      params: {
        id: _joi2.default.string().guid()
      }
    }
  }
}, {
  method: 'POST',
  path: '/fleets/{fleetId}/location',
  handler: LocationsController.post,
  config: {
    validate: {
      payload: {
        name: _joi2.default.string(),
        address: _joi2.default.string().min(8),
        postcode: _joi2.default.string().min(5).max(7),
        coords: _joi2.default.object().keys({
          lat: _joi2.default.number(), // add range validation -90 - 90
          lng: _joi2.default.number() // -180 - 180
        }),
        telephoneNumber: _joi2.default.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"'),
        extraInfo: _joi2.default.string().allow('').allow(null).default('')
      },
      params: {
        fleetId: _joi2.default.string().guid()
      }
    },
    auth: false
  }
}, {
  method: 'POST',
  path: '/fleets/{fleetId}/locations/add',
  handler: LocationsController.addNewLocationWithAuthentication,
  config: {
    validate: {
      payload: {
        name: _joi2.default.string(),
        address: _joi2.default.string().min(8),
        postcode: _joi2.default.string().min(5).max(7),
        coords: _joi2.default.object().keys({
          lat: _joi2.default.number(), // add range validation -90 - 90
          lng: _joi2.default.number() // -180 - 180
        }),
        telephoneNumber: _joi2.default.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"'),
        extraInfo: _joi2.default.string().allow('').allow(null).default('')
      },
      params: {
        fleetId: _joi2.default.string().guid()
      }
    }
  }
}, {
  method: 'PUT',
  path: '/fleet/locations/{locationId}',
  handler: LocationsController.update,
  config: {
    validate: {
      payload: {
        name: _joi2.default.string(),
        address: _joi2.default.string().min(8),
        postcode: _joi2.default.string().min(5).max(7),
        coords: _joi2.default.object().keys({
          lat: _joi2.default.number(),
          lng: _joi2.default.number()
        }),
        telephoneNumber: _joi2.default.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"'),
        extraInfo: _joi2.default.string().allow('').allow(null).default('')
      },
      params: {
        locationId: _joi2.default.string().guid()
      }
    }
  }
}, {
  method: 'DELETE',
  path: '/fleet/locations/{locationId}',
  handler: LocationsController.del,
  config: {
    validate: {
      params: {
        locationId: _joi2.default.string().guid()
      }
    }
  }
}, {
  method: 'GET',
  path: '/fleet/called/{called}',
  handler: FleetsController.getByCalled,
  config: {
    validate: {
      params: {
        called: _joi2.default.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"')
      }
    },
    auth: 'simple'
  }
}, {
  method: 'GET',
  path: '/fleets',
  handler: (0, _protect2.default)('super-admin', FleetsController.getAll)
}, {
  method: 'GET',
  path: '/fleet/locations',
  handler: LocationsController.getAllForFleet
}, {
  method: 'GET',
  path: '/activeusers',
  handler: (0, _protect2.default)('super-admin', Users.activeUsers)
}, {
  method: 'GET',
  path: '/locations',
  handler: (0, _protect2.default)('super-admin', LocationsController.getAll)
}, {
  method: 'PATCH',
  path: '/location/{id}', // update to /location/verify/{id}
  handler: LocationsController.verify,
  config: {
    validate: {
      params: {
        id: _joi2.default.string().guid()
      },
      payload: {
        oneTimeCode: _joi2.default.string().min(4)
      }
    },
    auth: false
  }
}, {
  method: 'POST',
  path: '/book/{locationId}',
  handler: BookingsController.book,
  config: {
    validate: {
      payload: {
        passengerName: _joi2.default.string().min(2).max(80),
        pickUpTime: _joi2.default.date().iso(),
        passengerCount: _joi2.default.number().integer().min(1).max(6),
        telephoneNumber: _joi2.default.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"')
      },
      params: {
        locationId: _joi2.default.string().min(6)
      }
    },
    auth: 'simple'
  }
}, {
  method: 'POST',
  path: '/booking/{pk}/cancel',
  handler: BookingsController.cancel,
  config: {
    validate: {
      payload: {
        telephoneNumber: _joi2.default.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"'),
        description: _joi2.default.string().allow('').default('Cancelled by GoCaller.co').optional()
      },
      params: {
        pk: _joi2.default.string().min(24)
      }
    },
    auth: 'simple'
  }
}, {
  method: 'GET',
  path: '/location/telephone/{number}',
  handler: LocationsController.getByTelephoneNumber,
  config: {
    validate: {
      params: {
        number: _joi2.default.string()
      }
    },
    auth: {
      strategies: ['simple']
    }
  }
}, {
  method: 'POST',
  path: '/identifycaller',
  handler: _identifyCaller2.default,
  config: {
    validate: {
      payload: {
        caller: _joi2.default.string(),
        called: _joi2.default.string()
      }
    },
    auth: {
      strategies: ['simple']
    }
  }
}, {
  method: 'POST',
  path: '/tdispatch/webhook',
  handler: _tdispatchWebhook2.default,
  config: {
    auth: false
  }
}, {
  method: 'GET',
  path: '/',
  handler: function handler(request, reply) {
    reply('OK');
  },
  config: {
    auth: false
  }
}, {
  method: 'GET',
  path: '/emails/welcome',
  handler: function handler(request, reply) {
    reply((0, _Email.welcomeTemplate)({
      link: 'https://app.gocaller.co.uk',
      name: 'Mike James',
      fleetName: 'Mikes Cabs'
    }));
  },
  config: {
    auth: false
  }
}, {
  method: 'GET',
  path: '/emails/forgot',
  handler: function handler(request, reply) {
    reply((0, _Email.forgotTemplate)({
      link: 'https://app.gocaller.co.uk/passwordreset/1a5ae654-6388-47f4-b7b4-641f01bfb248'
    }));
  },
  config: {
    auth: false
  }
}, {
  method: 'GET',
  path: '/fleet/twilio/numbers/search',
  config: {
    validate: {
      query: {
        isoCode: _joi2.default.string().min(2).max(2),
        number: _joi2.default.string()
      }
    }
  },
  handler: TwilioProxy.searchNumbers
}, {
  method: 'POST',
  path: '/fleet/twilio/numbers/purchase',
  config: {
    validate: {
      payload: {
        phoneNumber: _joi2.default.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"'),
        locationId: _joi2.default.string().guid().allow(null).default(null)
      }
    }
  },
  handler: TwilioProxy.purchaseNumber
}, {
  method: 'DELETE',
  path: '/fleet/twilio/numbers/release',
  config: {
    validate: {
      payload: {
        sid: _joi2.default.string()
      }
    }
  },
  handler: TwilioProxy.releaseNumber
}, {
  method: 'GET',
  path: '/fleet/twilio/account',
  handler: TwilioProxy.summary
}, {
  method: 'GET',
  path: '/fleet/twilio/numbers',
  handler: TwilioProxy.getNumbers
}, {
  method: 'POST',
  path: '/location/{id}/number/assign',
  handler: LocationsController.assignNumber,
  config: {
    validate: {
      payload: {
        number: _joi2.default.string().regex(/\+[0-9]*/, 'basic phone number prefixed with "+"')
      }
    }
  }
}];