// a functional approach to building out this api
// by no means complete, just an example on what a functional structure could yeild
const _ = require('lodash');
const curry = _.curry;
const rp = require('request-promise');

function newBooking(request, reply) {
  book(request.payload)
  .then((booking) => {

    reply();

  });

}

function book(booking) {

  getLocationByShortId(booking.locationId)
  .then((location) => {

    // no side effects
    const bookingPayload = Object.assign({}, booking, {
      location: location
    });

    // generic replacement
    return tdispatchBook(bookingPayload);

  });

}

function getLocationByShortId(shortId) {
  
  return new Promise((resolve, reject) => {

    Db.view('Location/getByShortId', {
      key: shortId
    }, (err, res) => {
      if (err) {
        return reject(err);
      }
      let resCloned;

      if (res && _.isArray(res) && !res.length) {
        reject({
          notFound: true
        });

      } else if (res && res.length) {

        resCloned = _.clone(_.first(res).value);
        resCloned.id = res.id;
        resolve(resCloned);
      } else {

        reject('unrecognised response');
      }
    });


  });
}

function tdispatchBook(booking) {
  const {credentials} = booking.location.fleet;

  auth(credentials)
  .then(curry(book)(booking));
}

function bookWithTDispatch(booking, token){
  return rp({
    method: 'POST',
    url: `${API_URL}/book`,
    body: JSON.stringify({
      passengerName: booking.passengerName,
      telephoneNumber: booking.telephoneNumber,
      postcode: booking.postcode,
      address: booking.address,
      pickUpTime: booking.pickUpTime
    })
  })
  .then(okResponse)
  .then(parseBody);
}

function auth(credentials) {
  return stepOne(credentials)
  .then(getTokens);
}

function stepOne(credentials) {
  return rp({
    url: `${API_URL}/auth`,
    body: JSON.stringify(credentials),
    method: 'POST'
  })
  .then(okResponse)
  .then(parseBody)
}

function getTokens(interMediateResponse) {
  return rp({
    url: `${API_URL}/gettokens`,
    method: 'GET'
  })
  .then(okResponse)
  .then(parseBody)
}